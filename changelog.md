# Released versions

## 0.0.4

### Added
- Scene representation on server side (players, monters)
- Level design updates (Plains, Wasteland and Mountains)
- Spawning monster on server via spawners prefab

### Changed
- 

### Removed
- 


## 0.0.3

### Added
- New scenes - City, Mountains, Plains and Wasteland
- In game menu with the option to quit or to load other scenes
- Server synchronization with scene loading across all clients

### Changed
- Send into game procedure. Now taking into account also clients, that are in the starting scene

### Removed
- Nav mesh agent from server's representation of Player Manager (not needed)

## 0.0.2

### Added
- Chat and messaging system

## 0.0.1

### Changed
- Migration of player movement to Client
- Camera view changed (third person now)
- Movement is now point to click as in Path of Exile

## 0.0.0

### Added
- Working infrastructure implemented (Server & Clients)
- Server is running on 1 thread
- TCP/UDP networking implemented -> Sending packets
- Basic character movement, synced over the network
- Basic character animation, synced over the network
- Test world with models created
