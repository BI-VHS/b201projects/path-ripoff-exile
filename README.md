# Path of Exile Custom Adventure

This is a landing page for our project. We are a team of 5 Czech students, that are developing a multiplayer RPG game. Below you can find our team members and their specialization.

This is a repository only for documentation and released builds. We are using Unity Teams (student licence) for development, so the project is hosted on Unity's cloud.

| Name              | Role                                  |
|-------------------|---------------------------------------|
| Marek Klofac      | Backend Unity programming, networking |
| Adam Polivka      | 3D model creation                     |
| Klara Matouskova  | Story writer                          |
| Tadeas Busek      | Level design                          |
| Matyas Sojka      | Sounds, design, graphics              |


## Documentation
- [Static world documentation](https://gitlab.fit.cvut.cz/BI-VHS/b201projects/path-ripoff-exile/blob/master/Documentation/static_world.adoc)
- [Dynamic world documentation](https://gitlab.fit.cvut.cz/BI-VHS/b201projects/path-ripoff-exile/blob/master/Documentation/dynamic_world.adoc)
- [Networking documentation](https://gitlab.fit.cvut.cz/BI-VHS/b201projects/path-ripoff-exile/blob/master/Documentation/networking.adoc)
- [Third iteration](https://gitlab.fit.cvut.cz/BI-VHS/b201projects/path-ripoff-exile/blob/master/Documentation/third_iteration.adoc)
- [Source code of project (latest version 0.0.4)](https://gitlab.fit.cvut.cz/BI-VHS/b201projects/path-ripoff-exile/blob/master/Sources)


## Useful links
- [Changelog](https://gitlab.fit.cvut.cz/BI-VHS/b201projects/path-ripoff-exile/blob/master/changelog.md)
- IP of our server hosted on Google Cloud - 35.207.144.140

## Requirements

### OS Requirements (both Client and Server):
- Windows 10 (played and tested)
- MacOS and Linux platform may be supported in later releases

### Using our hosted server:
- You can use our hosted server on Google Cloud platform with following IP address: 
- The server should be up and running 24/7 up until the 22 of april 2021
- After this date, get information about hosted server from our develop team

### Own server hosting:
- If you want to run server instance on your own, you can
- Simply just run /server_1.0.0/GameServer.exe application a be sure to add it to firewall exceptions
- Then PCs in the same subnet will be able to join the server (localhost - 127.0.0.1)

## Downloads

(1.1.0 RPP version) - [All](https://drive.google.com/drive/folders/1bR2SeLqH0BHwhAlZ9g58fFV0-Qqs9POh?usp=sharing)

(1.0.0 Alpha version) - [Server](https://drive.google.com/file/d/1VH8nSL0nUCbOhWjnF--m8-nh1pu2eVbZ/view?usp=sharing) | [Client](https://drive.google.com/file/d/1CC88qLy8Xf5HXphpKgCKqBj7YO1a4dXF/view?usp=sharing)

(0.0.5) - [Server](https://drive.google.com/file/d/1yFPVnpojp3VHnGoaMYle3eTA2RI-2IDq/view?usp=sharing) | [Client](https://drive.google.com/file/d/1VigeWabZjVk76jOejdgtDd8GswFnol4y/view?usp=sharing)

(0.0.4) - [Server](https://drive.google.com/file/d/1RGDYjevmpIUpulFnn5jJVIgxaw4hSAGG/view?usp=sharing) | [Client](https://drive.google.com/file/d/11fHVXWJMpCC_PFz_EE9AYeOB9ryUVG5S/view?usp=sharing)

(0.0.3) - [Server](https://drive.google.com/drive/folders/1nXSH7iy39YeptE42T--vS-PyG027yYFK?usp=sharing) | [Client](https://drive.google.com/drive/folders/1pNb8zyI0hXLwfJPKHknRmtGK3V5trTJm?usp=sharing)

(0.0.2) - [Server](https://drive.google.com/drive/folders/18J82QhtVZgV90CI4l1B6FuAMbOuZnKXo?usp=sharing) | [Client](https://drive.google.com/drive/folders/1UOmrBHspqtCA0BqIhmsxpuuAWBVs9Ccz?usp=sharing)

(0.0.1) - [Server](https://drive.google.com/drive/folders/1f0LJnD0Zu2SGMORJNIiJS8s44T6E5hH9?usp=sharing) | [Client](https://drive.google.com/drive/folders/1UvgWWuLxk0-rW6KFzhDYh27j_uyDtqg9?usp=sharing)

(0.0.0) - [Server](https://drive.google.com/drive/folders/1xy1r9Xas4WMkLcqGRf1rdHdx-RHPa33f?usp=sharing) | [Client](https://drive.google.com/drive/folders/1zJy709ox_HjlwJ91X-bdgEZRR4s2eXxW?usp=sharing)