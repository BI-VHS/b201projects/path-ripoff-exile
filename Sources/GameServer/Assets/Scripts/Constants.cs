﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public const int TICKS_PER_SEC = 30;
    public const int MS_PER_TICK = 1000 / TICKS_PER_SEC;
    public const SceneType STARTING_MAP = SceneType.City;

    public enum SceneType
    {
        Devroom,
        Mountains,
        City,
        Plains,
        Wasteland
    }

    public enum MonsterType
    {
        Goblin,
        Imp,
        Werewolf,
        Murlock,
        Flower,
        Cyclop,
        Ogre,
        Ent,
        Wolf,
        Iguana,
        IceGolem,
        Dog
    }

    public enum CharacterClass
    {
        None    = -1,
        Warrior = 0,
        Mage    = 1,
        Thief   = 2,
        Medic   = 3
    }
}
