﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;

public class Server
{
    // Class variables
    private static TcpListener tcpListener;
    private static UdpClient udpListener;
    public static int MaxPlayers { get; private set; }
    public static int Port { get; private set; }
    public static Dictionary<int, Client> clients = new Dictionary<int, Client>();
    public delegate void PacketHandler(int _fromClient, Packet _packet);
    public static Dictionary<int, PacketHandler> packetHandlers;

    // Starting the server with the number of maximum players and specific port
    public static void Start(int _maxPlayers, int _port)
    {
        MaxPlayers = _maxPlayers;
        Port = _port;

        Debug.Log("Starting server...");
        InitializeServerData();

        // Listening for incoming connections for TCP
        tcpListener = new TcpListener(IPAddress.Any, Port);
        tcpListener.Start();
        tcpListener.BeginAcceptTcpClient(new AsyncCallback(TCPConnectCallback), null);

        // Listening for incoming connections for UDP
        udpListener = new UdpClient(Port);
        udpListener.BeginReceive(UDPReceiveCallback, null);

        Debug.Log($"Server started on port: {Port}.");
    }

    // Prepares the server internal state by empty Client initialization 
    private static void InitializeServerData()
    {
        for (int i = 1; i <= MaxPlayers; i++)
        {
            clients.Add(i, new Client(i));
        }
        // Prepare the dictionary for server's packet handlers
        packetHandlers = new Dictionary<int, PacketHandler>()
            {
                {(int)ClientPackets.welcomeReceived, ServerHandle.WelcomeReceived},
                {(int)ClientPackets.playerMovement, ServerHandle.PlayerMovement},
                {(int)ClientPackets.sendChatMessage, ServerHandle.NewChatMessage},
                {(int)ClientPackets.changeScene, ServerHandle.ChangeScene},
                {(int)ClientPackets.dealDamageToMonster, ServerHandle.DealDamageToMonster},
                {(int)ClientPackets.playAnimation, ServerHandle.PlayAnimation},
                {(int)ClientPackets.playerDied, ServerHandle.PlayerDied},
                {(int)ClientPackets.playerRespawn, ServerHandle.PlayerRespawn},
            };
        Debug.Log("Initialized packets.");
    }

    // Called upon server got a connection from a client
    private static void TCPConnectCallback(IAsyncResult _result)
    {
        // Acknowledge the client and start the listening again for other clients
        TcpClient _client = tcpListener.EndAcceptTcpClient(_result);
        tcpListener.BeginAcceptTcpClient(TCPConnectCallback, null);

        Debug.Log($"Incoming connection from {_client.Client.RemoteEndPoint}...");

        // Register incoming client into one of the free slots
        for (int i = 1; i <= MaxPlayers; i++)
        {
            if (clients[i].tcp.socket == null)
            {
                clients[i].tcp.Connect(_client);
                return;
            }
        }
        // TODO: Send message to client that server is full
        Debug.Log($"{_client.Client.RemoteEndPoint} failed to connect: Server full!");
    }

    // Called upon server received data from a client 
    private static void UDPReceiveCallback(IAsyncResult _result)
    {
        try
        {
            // Obtain data and client's IP address
            IPEndPoint _clientEndPoint = new IPEndPoint(IPAddress.Any, 0);
            byte[] _data = udpListener.EndReceive(_result, ref _clientEndPoint);

            // Start listening again
            udpListener.BeginReceive(UDPReceiveCallback, null);

            // TODO: Possible source of errors when UDP packet segmentation occurs
            if (_data.Length < 4)
            {
                return;
            }

            using (Packet _packet = new Packet(_data))
            {
                int _clientId = _packet.ReadInt();

                // Client id doesn't exist in our dictionary
                if (_clientId == 0)
                {
                    return;
                }
                // New client, obtained FIRST message from him, initiate connection
                if (clients[_clientId].udp.endPoint == null)
                {
                    clients[_clientId].udp.Connect(_clientEndPoint);
                    return;
                }
                // Compare obtained and expected client id. Hacker proof
                if (clients[_clientId].udp.endPoint.ToString() == _clientEndPoint.ToString())
                {
                    // Handle obtained data
                    clients[_clientId].udp.HandleData(_packet);
                }
            }
        }
        catch (Exception _ex)
        {
            Debug.Log($"Error receiving UDP data: {_ex}");
        }
    }

    // Server sending data towards client via UDP
    public static void SendUDPData(IPEndPoint _clientEndPoint, Packet _packet)
    {
        try
        {
            // We have opened connection
            if (_clientEndPoint != null)
            {
                udpListener.BeginSend(_packet.ToArray(), _packet.Length(), _clientEndPoint, null, null);
            }
        }
        catch (Exception _ex)
        {
            Debug.Log($"Error sending data to {_clientEndPoint} via UDP: {_ex}");
        }
    }

    // Cleanup after the game is closed
    public static void Stop()
    {
        tcpListener.Stop();
        udpListener.Close();
    }
}
