﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Werewolf : MonsterManager
{
    public override void Move()
    {
        // Each x seconds move with randomly with this monster
        // Make new destination inside a circle around a default position
        Vector2 randomPoint = Random.insideUnitCircle * wanderingRadius;
        currentDestination = new Vector3(defaultPosition.x + randomPoint.x, currentDestination.y, defaultPosition.z + randomPoint.y);
    }
}