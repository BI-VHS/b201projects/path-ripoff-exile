﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MonsterManager : MonoBehaviour
{
    public Constants.MonsterType type;
    public int id;
    public int health;
    public float aggroRange;
    public float attackRange;
    public int attackDamage;
    public float attackDelay;
    public float movementSpeed;
    public bool hostile;
    public Vector3 currentDestination;
    public Vector3 defaultPosition;
    public float wanderingRadius;
    protected PlayerManager playerToAttack;
    private float nextAttackEvent;

    public virtual void Start()
    {
        hostile = false;
        playerToAttack = null;
    }

    public virtual void Update()
    {
        // Move towards our destination using lerp each frame
        float step =  movementSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(currentDestination.x, transform.position.y, currentDestination.z), step);
        
        // Check to see if we have a valid target to attack (player)
        if(playerToAttack != null)
        {
            // If player is in our attack range, attack it
            if(playerToAttack.Alive())
            {
                if(PlayerInAttackRange() && Time.time >= nextAttackEvent)
                {
                    nextAttackEvent = Time.time + attackDelay;
                    AttackTargetedPlayer();
                }
            }
            else
            {
                ResetPlayerToAttack();
            }
        }
    }

    public void TakeDamage(int _amount)
    {
        health -= _amount;
    }

    public void SetPlayerToAttack(PlayerManager _player)
    {
        hostile = true;
        playerToAttack = _player;
        currentDestination = playerToAttack.transform.position;
    }

    public void ResetPlayerToAttack()
    {
        hostile = false;
        playerToAttack = null;
    }

    // Return true if player to attack is in monster's aggro range
    public bool PlayerInAggroRange()
    {
        if(playerToAttack)
        {
            return Vector3.Distance(transform.position, playerToAttack.transform.position) <= aggroRange;
        }
        else
        {
            return false;
        }
    }

    public bool PlayerInAttackRange()
    {
        if(playerToAttack)
        {
            return Vector3.Distance(transform.position, playerToAttack.transform.position) <= attackRange;
        }
        else
        {
            return false;
        }
    }

    // Send info via server to target player that given monster attacked him
    public virtual void AttackTargetedPlayer()
    {
        ServerSend.MonsterAttacked(playerToAttack.id, id, attackDamage);
    }

    public abstract void Move();
}
