﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerSend
{
    // Send a TCP packet to ONE specific client
    private static void SendTCPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].tcp.SendData(_packet);
    }

    // Send a UDP packet to ONE specific client
    private static void SendUDPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].udp.SendData(_packet);
    }

    // Send a TCP packet to ALL clients on the server
    private static void SendTCPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            Server.clients[i].tcp.SendData(_packet);
        }
    }

    // Send a UDP packet to ALL clients on the server
    private static void SendUDPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            Server.clients[i].udp.SendData(_packet);
        }
    }

    // Send a TCP packet to ALL clients except one on the server
    private static void SendTCPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].tcp.SendData(_packet);
            }
        }
    }

    // Send a UDP packet to ALL clients except one on the server
    private static void SendUDPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].udp.SendData(_packet);
            }
        }
    }

    // Test welcome packet, using block for automatic clean discard
    public static void Welcome(int _toClient, string _msg)
    {
        using (Packet _packet = new Packet((int)ServerPackets.welcome))
        {
            _packet.Write(_msg);
            _packet.Write(_toClient);
            SendTCPData(_toClient, _packet);
        }
    }

    // Informing client about it's PlayerManager's attributes
    public static void SpawnPlayer(int _toClient, PlayerManager _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.spawnPlayer))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.username);
            _packet.Write((int) _player.characterClass);
            _packet.Write(_player.transform.position);
            _packet.Write(Quaternion.identity);
            SendTCPData(_toClient, _packet);
        }
    }

    // Deleting a client for another client because the first one left the scene
    public static void PlayerLeftScene(int _toClient, int _clientThatLeft)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerLeftScene))
        {
            _packet.Write(_clientThatLeft);
            SendTCPData(_toClient, _packet);
        }
    }

    // One of the server's clients has moved and we are informing all the other about his new position
    public static void PlayerMoved(int _playerId, Vector3 _newPosition)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerMoved))
        {
            _packet.Write(_playerId);
            _packet.Write(_newPosition);

            // All except the one that moved
            SendUDPDataToAll(_playerId, _packet);
        }
    }

    // One of the server's clients has moved and we are informing all the other about his new position
    public static void MonsterMoved(int _toClient, MonsterManager _monster)
    {
        using (Packet _packet = new Packet((int)ServerPackets.monsterMoved))
        {
            _packet.Write(_monster.id);
            _packet.Write(_monster.transform.position);

            SendUDPData(_toClient, _packet);
        }
    }

    // One of the server's clients has posted new message in chat. We as a dumb server
    // inform all others about his message, not keeping any history on the server.
    public static void NewChatMessage(int _fromClient, string _date, string _username, string _text)
    {
        using (Packet _packet = new Packet((int)ServerPackets.newChatMessage))
        {
            _packet.Write(_date);
            _packet.Write(_username);
            _packet.Write(_text);
            SendTCPDataToAll(_fromClient, _packet);
        }
    }

    // Sending our client his new coordinates in requested new scene
    public static void ChangeSceneSpawnPoint(int _toClient, Vector3 spawnPoint)
    {
        using (Packet _packet = new Packet((int)ServerPackets.changeSceneSpawnPoint))
        {
            _packet.Write(spawnPoint);
            SendTCPData(_toClient, _packet);
        }
    }

    // Informing only one client that new monster has been spawned
    // Used when changing the scene and informing player about already existing monsters
    public static void SpawnMonster(int _toClient, MonsterManager _monster)
    {
        using (Packet _packet = new Packet((int)ServerPackets.spawnMonster))
        {
            _packet.Write((int) _monster.type);
            _packet.Write(_monster.id);
            _packet.Write(_monster.transform.position);
            _packet.Write(Quaternion.identity);
            SendTCPData(_toClient, _packet);
        }
    }

    // Monster has been hit by client. Server has calculated new health values. Send it to all clients
    public static void MonsterTookDamage(int _monsterId, int _dealtDamage)
    {
        using (Packet _packet = new Packet((int)ServerPackets.monsterTookDamage))
        {
            _packet.Write(_monsterId);
            _packet.Write(_dealtDamage);
            SendUDPDataToAll(_packet);
        }
    }

    // Monster has been killed by client. Send it to all clients
    public static void MonsterDied(int _monsterId, int _playerThatKilledMonster)
    {
        using (Packet _packet = new Packet((int)ServerPackets.monsterDied))
        {
            _packet.Write(_monsterId);
            _packet.Write(_playerThatKilledMonster);
            SendUDPDataToAll(_packet);
        }
    }

    // One of our clients has played animation a we are informing all others so that they can also play the animation
    public static void PlayerPlayedAnimation(int _toClient, int _playerThatPlayed, string _animation)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerPlayedAnimation))
        {
            _packet.Write(_playerThatPlayed);
            _packet.Write(_animation);
            SendUDPData(_toClient, _packet);
        }
    }

    // Given monster by its id has attacked target player. Inform the client about this attack
    public static void MonsterAttacked(int _attackedPlayerId, int _monsterId, int _attackDamage)
    {
        using (Packet _packet = new Packet((int)ServerPackets.monsterAttacked))
        {
            _packet.Write(_attackedPlayerId);
            _packet.Write(_monsterId);
            _packet.Write(_attackDamage);
            SendUDPDataToAll(_packet);
        }
    }

    // Inform target client that he has been warped to target location
    public static void PlayerWarped(int _playerId, Vector3 _spawnPoint)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerWarped))
        {
            _packet.Write(_playerId);
            _packet.Write(_spawnPoint);
            SendTCPDataToAll(_packet);
        }
    }

    // Informing still all connected clients, that one of them left
    public static void PlayerDisconnected(int _playerId)
    {
        using(Packet _packet = new Packet((int)ServerPackets.playerDisconnected))
        {
            _packet.Write(_playerId);
            SendTCPDataToAll(_packet);
        }
    }
}
