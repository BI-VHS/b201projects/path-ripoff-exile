﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public Constants.SceneType type;
    public Vector3 playerSpawnPoint;
    public bool spawnable;
    public List<PlayerManager> players;
    public List<MonsterManager> monsters;
    public MonsterSpawner [] monsterSpawners;
    public float resetMonsterSpawnersDelay;
    private float wanderingDelay;
    private float nextMoveEvent;
    private float monsterMoveTimeout = 1.0f;

    public void Start()
    {
        resetMonsterSpawnersDelay = 300f;
        wanderingDelay = 3f;
        monsterSpawners = GetComponentsInChildren<MonsterSpawner>();
        StartCoroutine(ResetMonsterSpawners(resetMonsterSpawnersDelay));
        StartCoroutine(MoveMonsters(wanderingDelay));
    }

    public void Update()
    {
        // For each monster, check to see if any player is closer than monster's attack range
        foreach(MonsterManager monster in monsters)
        {
            foreach(PlayerManager player in players)
            {
                if(Vector3.Distance(monster.transform.position, player.transform.position) <= monster.aggroRange && player.Alive())
                {
                    // Set current player as attack target for monster
                    monster.SetPlayerToAttack(player);
                    break;
                }
            }
            HandleMonsterAggro(monster);
        }
    }

    private void HandleMonsterAggro(MonsterManager _monster)
    {
        // If monster has found valid player as target, move it towards the player
        if(_monster.PlayerInAggroRange() && Time.time >= nextMoveEvent)
        {
            // Inform all Client's players that are in the same scene, that monster has moved
            foreach(PlayerManager player in players)
            {
                ServerSend.MonsterMoved(player.id, _monster);
            }
            nextMoveEvent = Time.time + monsterMoveTimeout;
        }
        else
        {
            // We lost our target player. He escaped our aggro range
            _monster.ResetPlayerToAttack();
        }
    }

    public void AddPlayer(PlayerManager _player)
    {
        players.Add(_player);
    }

    public void RemovePlayer(PlayerManager _player)
    {
        players.Remove(_player);
    }

    public void RemoveMonster(MonsterManager _monster)
    {
        monsters.Remove(_monster);
    }

    public bool ContainsPlayer(PlayerManager _player)
    {
        return players.Contains(_player);
    }

    // Each x seconds check for disabled spawners and activate them.
    // They spawn all their's monters and return them
    // Inform all other clients in this scene, that new monster has spawned.
    private IEnumerator ResetMonsterSpawners(float _delay)
    {
        Debug.Log("[" + type.ToString() + "] Trying to reset all spawners");
        foreach(MonsterSpawner spawner in monsterSpawners)
        {
            if(!spawner.IsActive())
            {
                List<MonsterManager> spawnedMonsters = spawner.Spawn();
                foreach(MonsterManager monster in spawnedMonsters)
                {
                    monsters.Add(monster);
                    // Inform all Client's players that are in the same scene, that monster has spawned
                    foreach (PlayerManager player in players)
                    {
                        ServerSend.SpawnMonster(player.id, monster);
                    }
                }
            }
        }
        // Wait for x seconds before starting again
        yield return new WaitForSeconds(_delay);

        // Start again
        StartCoroutine(ResetMonsterSpawners(_delay));
    }

    // Randomly choosing, which monster to move with. They are oscilating around a fixed point.
    private IEnumerator MoveMonsters(float _delay)
    {
        foreach(MonsterManager monster in monsters)
        {
            if(Random.Range(0, 10) % 2 == 0 && !monster.hostile)
            {
                monster.Move();

                // Inform all Client's players that are in the same scene, that monster has moved
                foreach(PlayerManager player in players)
                {
                    ServerSend.MonsterMoved(player.id, monster);
                }
            }
        }

        // Wait for x seconds before starting again
        yield return new WaitForSeconds(_delay);

        // Start again
        StartCoroutine(MoveMonsters(_delay));
    }
}
