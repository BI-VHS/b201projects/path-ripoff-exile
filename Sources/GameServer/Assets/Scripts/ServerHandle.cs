﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerHandle
{
    public static void WelcomeReceived(int _fromClient, Packet _packet)
    {
        int _clientIdCheck = _packet.ReadInt();
        string _username = _packet.ReadString();
        Constants.CharacterClass characterClass = (Constants.CharacterClass) _packet.ReadInt();

        Debug.Log($"{Server.clients[_fromClient].tcp.socket.Client.RemoteEndPoint} [{_username}] connected succesfully and is now player {_fromClient}.");
        
        // Not maching client ID, man in the middle attack?
        if (_fromClient != _clientIdCheck)
        {
            Debug.Log($"Player \"{_username}\" (ID: {_fromClient}) has assumed the wrong client ID ({_clientIdCheck})! Man in the middle attack!");
        }

        // Send player into the game
        Server.clients[_fromClient].SendIntoGame(_username, characterClass);
    }

    // Obtained new position from some client. Update internal representation and send to all other clients
    public static void PlayerMovement(int _fromClient, Packet _packet)
    {
        // Parse the data obtained packet
        Vector3 newDestination = _packet.ReadVector3();
        
        // Update client with obtained data
        Server.clients[_fromClient].player.UpdatePosition(newDestination);

        // Send data to all other clients on the server about this change
        ServerSend.PlayerMoved( _fromClient, newDestination);
    }

    // One of the clients sent new message into the chat. Inform other clients.
    // Chat is only client sided, as of now no history of message here on server
    // Server is only a relay for chat messages
    public static void NewChatMessage(int _fromClient, Packet _packet)
    {
        string date = _packet.ReadString();
        string username = _packet.ReadString();
        string text = _packet.ReadString();

        ServerSend.NewChatMessage(_fromClient, date, username, text);
    }

    public static void ChangeScene(int _clientThatLeft, Packet _packet)
    {
        // Parse new scene type from packet and prepare reference to Client's player instance
        Constants.SceneType newSceneType = (Constants.SceneType) _packet.ReadInt();
        PlayerManager playerThatLeft = Server.clients[_clientThatLeft].player;

        // Lookup current and new scene via Client's player, that initiated the change
        SceneManager currentScene = NetworkManager.instance.scenes.Find(scene => scene.ContainsPlayer(playerThatLeft));
        SceneManager newScene = NetworkManager.instance.scenes.Find(scene => scene.type == newSceneType);

        // Make the transition between scenes, updating server side scene representation
        NetworkManager.instance.PlayerChangedScene(playerThatLeft, currentScene, newScene);

        // Update players position in new scene SERVER-SIDE
        playerThatLeft.UpdatePosition(newScene.playerSpawnPoint);

        // Update players position in new scene CLIENT-SIDE
        ServerSend.ChangeSceneSpawnPoint(_clientThatLeft, newScene.playerSpawnPoint);

        // Delete the Client's player from all other Clients that were in the same scene with him
        // And Add Client's player to all CLients, that are in the new scene with him
        foreach(Client _client in Server.clients.Values)
        {
            // All clients without me
            if (_client.player != null && _client.id != _clientThatLeft)
            {
                // Prepare references to current scene in which is current Client's player
                SceneManager clientScene = NetworkManager.instance.scenes.Find(scene => scene.ContainsPlayer(_client.player));
                
                // Client's player that left was in our scene, 
                if(clientScene.type == currentScene.type)
                {
                    // Send delete request
                    ServerSend.PlayerLeftScene(_client.id, _clientThatLeft);
                }
                // Client's player has joined our scene, cross spawn us and him
                if(clientScene.type == newScene.type)
                {
                    // Spawn others to myself
                    ServerSend.SpawnPlayer(_clientThatLeft, _client.player);

                    // Spawn myself to others
                    ServerSend.SpawnPlayer(_client.id, playerThatLeft);
                }
            }
        }
        // Spawn all monsters that are in the new scene with Client's player that left
        foreach(MonsterManager monster in newScene.monsters)
        {
            ServerSend.SpawnMonster(_clientThatLeft, monster);
        }
    }

    // One of our clients has died and want to respawn in his scene
    public static void PlayerRespawn(int _fromClient, Packet _packet)
    {
        // Lookup scene where player that wants to respawn is
        PlayerManager player = Server.clients[_fromClient].player;
        SceneManager scene = NetworkManager.instance.scenes.Find(s => s.ContainsPlayer(player));

        player.Respawn(scene.playerSpawnPoint);
        ServerSend.PlayerWarped(_fromClient, scene.playerSpawnPoint);
    }

    // One of the clients has attacked a monster with given damage. Update appropriate
    // monster health. Send to all clients that are in the same scene as the one that send 
    // this message new monster health.
    // Also send information to them, that attack animation should be played
    public static void DealDamageToMonster(int _fromClient, Packet _packet)
    {
        int monsterId = _packet.ReadInt();
        int dealtDamage = _packet.ReadInt();

        // Player manager of client that has attacked the monster
        PlayerManager playerManager = Server.clients[_fromClient].player;

        // Find scene, where is the client that attacked the monster in same scene
        SceneManager scene = NetworkManager.instance.scenes.Find(s => s.ContainsPlayer(playerManager));

        // Find monster and update its health
        MonsterManager monster = scene.monsters.Find(m => m.id == monsterId);

        // Check to see if monster still exists on server. Fix when players could click on dead monster attacking them
        if(monster != null)
        {
            // Monster died
            if(monster.health - dealtDamage <= 0)
            {
                // Inform all clients that this monster died
                ServerSend.MonsterDied(monsterId, _fromClient);

                // Delete from local representation
                NetworkManager.instance.RemoveMonsterFromScene(monster, scene);
            }
            // Monster survived
            else
            {
                // Update server side
                monster.TakeDamage(dealtDamage);

                // Update all clients values
                ServerSend.MonsterTookDamage(monsterId, dealtDamage);
            }
        }
    }

    // One of our clients has played their animation, and requested us to echo it out to all
    // other clients. Just do it :P
    public static void PlayAnimation(int _fromClient, Packet _packet)
    {
        string animation = _packet.ReadString();

        // Player manager of client that has attacked the monster
        PlayerManager playerManager = Server.clients[_fromClient].player;

        // Find scene, where is the client
        SceneManager scene = NetworkManager.instance.scenes.Find(s => s.ContainsPlayer(playerManager));

        // Relay obtained info to all opther clients in the same scene except the one who sent it (he played his own animation already)
        foreach(PlayerManager p in scene.players)
        {
            if(p.id != _fromClient)
            {
                ServerSend.PlayerPlayedAnimation(p.id, _fromClient, animation);
            }
        }
    }

    // One of our clients has died in a scene
    public static void PlayerDied(int _fromClient, Packet _packet)
    {
        // Find appropriate player manager that's id matches with from client id
        PlayerManager playerManager = Server.clients[_fromClient].player;
        playerManager.Die();
    }
}
