﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerManager : MonoBehaviour
{
    public int id;
    public string username;
    public float movementSpeed;
    public Vector3 currentDestination;
    public Constants.CharacterClass characterClass;
    private bool alive = true;

    public void Update()
    {
        // Move towards our destination using lerp each frame
        float step =  movementSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, currentDestination, step);
    }

    public void UpdateDestination(Vector3 _newDestination)
    {
        currentDestination = _newDestination;
    }

    public void UpdatePosition(Vector3 _newPosition)
    {
        transform.position = _newPosition;
        currentDestination = _newPosition;
    }

    public bool Alive()
    {
        return alive;
    }

    public void Die()
    {
        alive = false;
    }

    public void Respawn(Vector3 _spawnPoint)
    {
        alive = true;
        UpdatePosition(_spawnPoint);
    }
}
