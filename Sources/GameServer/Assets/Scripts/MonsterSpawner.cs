﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class MonsterSpawner : MonoBehaviour
{
    public GameObject monsterPrefab;
    public Vector3 spawnPosition;
    public int monsterCount;
    public float wanderingRadius;
    private int currentMonsterCount;
    private bool active;

    // Start is called before the first frame update
    void Start()
    {
        Assert.IsNotNull(monsterPrefab);
    }

    // Decide whether the spawner is active or not by the number of children this object has
    void Update()
    {
        currentMonsterCount = this.gameObject.transform.childCount;
        active = currentMonsterCount == 0 ? false : true;
    }

    public List<MonsterManager> Spawn()
    {
        List<MonsterManager> spawnedMonsters = new List<MonsterManager>();
        for(int i=0; i<monsterCount; i++)
        {
            GameObject monster = Instantiate(monsterPrefab, spawnPosition, Quaternion.identity, this.gameObject.transform);
            MonsterManager monsterManager = monster.GetComponent<MonsterManager>();
            monsterManager.id = Random.Range(0, 2000000000);
            monsterManager.defaultPosition = spawnPosition;
            monsterManager.currentDestination = spawnPosition;
            monsterManager.wanderingRadius = wanderingRadius;
            spawnedMonsters.Add(monsterManager);
        }
        return spawnedMonsters;
    }

    public bool IsActive()
    {
        return active;
    }
}
