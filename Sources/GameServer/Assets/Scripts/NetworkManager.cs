﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[System.Serializable]
public class NetworkManager : MonoBehaviour
{
    public static NetworkManager instance;
    public List<SceneManager> scenes;
    public GameObject scenePrefab;
    public GameObject playerPrefab;

    // Initialization, using singleton here (only one instance throught out hte program)
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    private void Start()
    {
        Assert.IsNotNull(scenePrefab);
        Assert.IsNotNull(playerPrefab);

        // Server doesn't need to render images, constrain it. It should match tick rate
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;
        SceneManager [] arr = GetComponentsInChildren<SceneManager>();
        scenes = new List<SceneManager>(arr);
        Server.Start(50, 26950);
    }

    private void OnApplicationQuit()
    {
        Server.Stop();
    }

    public PlayerManager RegisterPlayer(int _id, string _username, Vector3 _position, Constants.CharacterClass _characterClass)
    {
        // Find starting scene
        SceneManager sceneManager = scenes.Find(scene => scene.type == Constants.STARTING_MAP);

        GameObject player = Instantiate(playerPrefab, _position, Quaternion.identity, sceneManager.gameObject.transform);
        PlayerManager playerManager = player.GetComponent<PlayerManager>();
        playerManager.id = _id;
        playerManager.username = _username;
        // playerManager.transform.position = _position;
        playerManager.characterClass = _characterClass;

        // Add player to the starting scene
        sceneManager.AddPlayer(playerManager);

        return playerManager;
    }

    public void DisconnectPlayer(PlayerManager _player)
    {
        SceneManager inScene = scenes.Find(scene => scene.ContainsPlayer(_player));
        inScene.RemovePlayer(_player);
        Destroy(_player.gameObject);
    }

    public void PlayerChangedScene(PlayerManager _player, SceneManager _currentScene, SceneManager _newScene)
    {
        _currentScene.RemovePlayer(_player);
        Debug.Log("[" + _currentScene.type.ToString() + "] Player has left the scene -> " + _player.username);

        _newScene.AddPlayer(_player);
        Debug.Log("[" + _newScene.type.ToString() + "] Player has joined the scene -> " + _player.username);

        // Change parents
        _player.gameObject.transform.SetParent(_newScene.gameObject.transform);
    }

    public void RemoveMonsterFromScene(MonsterManager _monster, SceneManager _scene)
    {
        _scene.RemoveMonster(_monster);
        Destroy(_monster.gameObject);
    }
}
