﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager instance;
    public GameObject playerPrefab;

    // Initialization, using singleton here (only one instance throught out hte program)
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    private void Start()
    {
        // Server doesn't need to render images, constrain it. It should match tick rate
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;
        Server.Start(50, 26950);
    }

    private void OnApplicationQuit()
    {
        Server.Stop();
    }

    // Creating new game object in Unity of player
    public Player InstantiatePlayer()
    {
        // Spawning player at these coordinates
        return Instantiate(playerPrefab, new Vector3(0f, 4.0f, 0f), Quaternion.identity).GetComponent<Player>();
    }
}
