﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerHandle
{
    public static void WelcomeReceived(int _fromClient, Packet _packet)
    {
        int _clientIdCheck = _packet.ReadInt();
        string _username = _packet.ReadString();

        Debug.Log($"{Server.clients[_fromClient].tcp.socket.Client.RemoteEndPoint} [{_username}] connected succesfully and is now player {_fromClient}.");
        // Not maching client ID, man in the middle attack?
        if (_fromClient != _clientIdCheck)
        {
            Debug.Log($"Player \"{_username}\" (ID: {_fromClient}) has assumed the wrong client ID ({_clientIdCheck})! Man in the middle attack!");
        }
        // Send player into the game
        Server.clients[_fromClient].SendIntoGame(_username);
    }

    // Obtained new position from some client. Update internal representation and send to all other clients
    public static void PlayerMovement(int _fromClient, Packet _packet)
    {
        // Parse the data obtained packet
        Vector3 newDestination = _packet.ReadVector3();
        
        // Update client with obtained data
        Server.clients[_fromClient].player.UpdateDestination(newDestination);

        // Send data to all other clients on the server about this change
        ServerSend.PlayerMoved( _fromClient, newDestination);
    }

    // One of the clients sent new message into the chat. Inform other clients.
    // Chat is only client sided, as of now no history of message here on server
    // Server is only a relay for chat messages
    public static void NewChatMessage(int _fromClient, Packet _packet)
    {
        string date = _packet.ReadString();
        string username = _packet.ReadString();
        string text = _packet.ReadString();

        ServerSend.NewChatMessage(_fromClient, date, username, text);
    }
}
