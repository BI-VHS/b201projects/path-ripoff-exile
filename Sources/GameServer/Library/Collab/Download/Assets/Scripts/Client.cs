﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;

public class Client
{
    // Class variables
    public static int dataBufferSize = 4096;
    public int id;
    public Player player;
    public TCP tcp;
    public UDP udp;

    // Constructor
    public Client(int _clientId)
    {
        id = _clientId;
        tcp = new TCP(id);
        udp = new UDP(id);
    }

    // -------------------------------------------------------------------------------
    // Helper subclass for TCP/IP networking stuff
    public class TCP
    {
        // Class variables
        public TcpClient socket;
        private readonly int id;
        private NetworkStream stream;
        private Packet receivedData;
        private byte[] receiveBuffer;

        // Constructor
        public TCP(int _id)
        {
            id = _id;
        }

        // SERVER-SIDE client connection to server
        public void Connect(TcpClient _socket)
        {
            // Socket initialization
            socket = _socket;
            socket.ReceiveBufferSize = dataBufferSize;
            socket.SendBufferSize = dataBufferSize;

            // Opening network stream a starting reading the data
            stream = socket.GetStream();
            receivedData = new Packet();
            receiveBuffer = new byte[dataBufferSize];
            stream.BeginRead(receiveBuffer, 0, dataBufferSize, ReceiveCallback, null);

            // Requesting a welcome message from the server, testing connection
            ServerSend.Welcome(id, "Welcome to the server!");
        }

        // Writing obtained packet into the socket's network stream
        public void SendData(Packet _packet)
        {
            try
            {
                if (socket != null)
                {
                    stream.BeginWrite(_packet.ToArray(), 0, _packet.Length(), null, null);
                }
            }
            catch (Exception _ex)
            {
                Debug.Log($"Error sending data to player {id} via TCP: {_ex}");
            }
        }

        // Called upon obtaining some kind of data by the server
        private void ReceiveCallback(IAsyncResult _result)
        {
            try
            {
                // Number of bytes read
                int _byteLength = stream.EndRead(_result);
                if (_byteLength <= 0)
                {
                    Server.clients[id].Disconnect();
                    return;
                }
                // Reallocate obtained data to our internal buffer
                byte[] _data = new byte[_byteLength];
                Array.Copy(receiveBuffer, _data, _byteLength);

                // Handle received data. Reset if all bytes arrived have been handled
                receivedData.Reset(HandleData(_data));

                // Data handled, read again
                stream.BeginRead(receiveBuffer, 0, dataBufferSize, ReceiveCallback, null);
            }
            catch (Exception _ex)
            {
                Debug.Log($"Error receiving TCP data: {_ex}");
                Server.clients[id].Disconnect();
            }
        }

        // This is pretty complicated function.
        // Basically it makes sure, that the data is handled correctly, if network
        // segmentation happens and for example we only receive a half of a packet.
        private bool HandleData(byte[] _data)
        {
            int _packetLength = 0;
            receivedData.SetBytes(_data);

            if (receivedData.UnreadLength() >= 4)
            {
                _packetLength = receivedData.ReadInt();
                // Reset received data
                if (_packetLength <= 0)
                {
                    return true;
                }
            }
            // Received data contains another full packet that we can handle
            while (_packetLength > 0 && _packetLength <= receivedData.UnreadLength())
            {
                // Copy bytes from another packet to temporary byte array
                byte[] _packetBytes = receivedData.ReadBytes(_packetLength);

                // Make sure, that this code runs on ONE thread using our thread manager
                ThreadManager.ExecuteOnMainThread(() =>
                {
                    // Load next packet from socket. Select appropriate packet handler
                    using (Packet _packet = new Packet(_packetBytes))
                    {
                        int _packetId = _packet.ReadInt();
                        Server.packetHandlers[_packetId](id, _packet);
                    }
                });
                // Reset packet length
                _packetLength = 0;

                // Check again if there are any other packets to be handled else return true for
                // received data to reset causing getting more data
                if (receivedData.UnreadLength() >= 4)
                {
                    _packetLength = receivedData.ReadInt();
                    // Reset received data
                    if (_packetLength <= 0)
                    {
                        return true;
                    }
                }
            }
            if (_packetLength <= 1)
            {
                return true;
            }
            // DON'T reset received data, some part of packet still left
            return false;
        }

        // Disconnection of a client from server
        public void Disconnect()
        {
            socket.Close();
            stream = null;
            receivedData = null;
            receiveBuffer = null;
            socket = null;
        }
    }

    // -------------------------------------------------------------------------------
    // Helper subclass for UDP/IP networking stuff
    public class UDP
    {
        public IPEndPoint endPoint;
        private int id;

        // Constructor
        public UDP(int _id)
        {
            id = _id;
        }

        public void Connect(IPEndPoint _endPoint)
        {
            endPoint = _endPoint;
        }

        public void SendData(Packet _packet)
        {
            Server.SendUDPData(endPoint, _packet);
        }

        public void HandleData(Packet _packetData)
        {
            // Slice out obtained bytes from packet
            int _packetLength = _packetData.ReadInt();
            byte[] _packetBytes = _packetData.ReadBytes(_packetLength);

            ThreadManager.ExecuteOnMainThread(() =>
            {
                // Assign appropriate packet handler to packet
                using (Packet _packet = new Packet(_packetBytes))
                {
                    int _packetId = _packet.ReadInt();
                    Server.packetHandlers[_packetId](id, _packet);
                }
            });
        }

        // Disconnection of a client from server
        public void Disconnect()
        {
            endPoint = null;
        }
    }

    // Handle all things around connecting the player into the game world
    // Cross-joining and registering players x players
    public void SendIntoGame(string _playerName)
    {
        player = NetworkManager.instance.InstantiatePlayer();
        player.Initialize(id, _playerName);

        // Spawning myself to other players
        foreach (Client _client in Server.clients.Values)
        {
            if (_client.player != null)
            {
                if (_client.id != id)
                {
                    ServerSend.SpawnPlayer(id, _client.player);
                }
            }
        }
        // Spawning other players for myself
        foreach (Client _client in Server.clients.Values)
        {
            if (_client.player != null)
            {
                ServerSend.SpawnPlayer(_client.id, player);
            }
        }
    }

    // Handle the discconnect of a client form server
    private void Disconnect()
    {
        Debug.Log($"{tcp.socket.Client.RemoteEndPoint} has disconnected from the server.");

        // Safely destroy client's gameobject
        ThreadManager.ExecuteOnMainThread(() =>
        {
            UnityEngine.Object.Destroy(player.gameObject);
            player = null;
        });
        tcp.Disconnect();
        udp.Disconnect();
        // Send packet to other servers clients that I am disconnecting
        ServerSend.PlayerDisconnected(id);
    }
}
