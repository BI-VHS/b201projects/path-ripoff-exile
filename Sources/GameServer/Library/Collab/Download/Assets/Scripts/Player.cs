﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour
{
    public int id;
    public string username;
    public NavMeshAgent navMeshAgent;

    public void FixedUpdate()
    {
        Vector3 forward = transform.TransformDirection(Vector3.forward) * 2;
        Debug.DrawRay(transform.position, forward, Color.green);
    }

    // Constructor
    public void Initialize(int _id, string _username)
    {
        id = _id;
        username = _username;
    }

    // Updates the target destination for nav mesh agent
    public void UpdateDestination(Vector3 _newDestination)
    {
        navMeshAgent.destination = _newDestination;
    }
}
