﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerSend
{
    // Send a TCP packet to ONE specific client
    private static void SendTCPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].tcp.SendData(_packet);
    }

    // Send a UDP packet to ONE specific client
    private static void SendUDPData(int _toClient, Packet _packet)
    {
        _packet.WriteLength();
        Server.clients[_toClient].udp.SendData(_packet);
    }

    // Send a TCP packet to ALL clients on the server
    private static void SendTCPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            Server.clients[i].tcp.SendData(_packet);
        }
    }

    // Send a UDP packet to ALL clients on the server
    private static void SendUDPDataToAll(Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            Server.clients[i].udp.SendData(_packet);
        }
    }

    // Send a TCP packet to ALL clients except one on the server
    private static void SendTCPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].tcp.SendData(_packet);
            }
        }
    }

    // Send a UDP packet to ALL clients except one on the server
    private static void SendUDPDataToAll(int _exceptClient, Packet _packet)
    {
        _packet.WriteLength();
        for (int i = 1; i <= Server.MaxPlayers; i++)
        {
            if (i != _exceptClient)
            {
                Server.clients[i].udp.SendData(_packet);
            }
        }
    }

    // Test welcome packet, using block for automatic clean discard
    public static void Welcome(int _toClient, string _msg)
    {
        using (Packet _packet = new Packet((int)ServerPackets.welcome))
        {
            _packet.Write(_msg);
            _packet.Write(_toClient);
            SendTCPData(_toClient, _packet);
        }
    }

    // Informing client about it's Player's attributes
    public static void SpawnPlayer(int _toClient, Player _player)
    {
        using (Packet _packet = new Packet((int)ServerPackets.spawnPlayer))
        {
            _packet.Write(_player.id);
            _packet.Write(_player.username);
            _packet.Write(_player.transform.position);
            _packet.Write(_player.transform.rotation);
            SendTCPData(_toClient, _packet);
        }
    }

    // One of the server's clients has moved and we are informing all the other about his new position
    public static void PlayerMoved(int _playerId, Vector3 _newDestination)
    {
        using (Packet _packet = new Packet((int)ServerPackets.playerMoved))
        {
            _packet.Write(_playerId);
            _packet.Write(_newDestination);

            // All except the one that moved
            SendUDPDataToAll(_playerId, _packet);
        }
    }

    // One of the server's clients has posted new message in chat. We as a dumb server
    // inform all others about his message, not keeping any history on the server.
    public static void NewChatMessage(int _fromClient, string _date, string _username, string _text)
    {
        using (Packet _packet = new Packet((int)ServerPackets.newChatMessage))
        {
            _packet.Write(_date);
            _packet.Write(_username);
            _packet.Write(_text);
            SendTCPDataToAll(_fromClient, _packet);
        }
    }

    // Informing still all connected clients, that one of them left
    public static void PlayerDisconnected(int _playerId)
    {
        using(Packet _packet = new Packet((int)ServerPackets.playerDisconnected))
        {
            _packet.Write(_playerId);
            SendTCPDataToAll(_packet);
        }
    }
}
