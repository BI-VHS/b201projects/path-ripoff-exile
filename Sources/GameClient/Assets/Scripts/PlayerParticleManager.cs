﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParticleManager : MonoBehaviour
{
    public ParticleSystem [] particles;
    public void PlayParticle(int ability)
    {
        particles[ability].Play();
    }
    public void StopParticle(int ability)
    {
        particles[ability].Stop();
    }
    public void StopAll()
    {
        foreach(ParticleSystem p in particles)
            p.Stop();
    }
}
