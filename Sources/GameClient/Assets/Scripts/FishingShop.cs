﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishingShop : MonoBehaviour
{
    public string relatedObjectName;
    private List<GameObject> children;
    private int idx = 0;
    void Start()
    {
        children = new List<GameObject>();

        foreach(Transform child in transform)
        {
            child.gameObject.SetActive(false);
            children.Add(child.gameObject);
        }
        children[idx].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        GameObject obj = GameObject.Find(relatedObjectName);
        bool isSelling = obj.GetComponent<Fishing>().isSelling;

        if(isSelling)
        {
            children[0].SetActive(true);
            children[1].SetActive(false);
        }
        else
        {
            children[0].SetActive(false);  
            children[1].SetActive(true);
        }

    }
}
