﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ability
{
    public int cost;
    public float time; //last time when ability was used
    public ParticleSystem particle;
}
