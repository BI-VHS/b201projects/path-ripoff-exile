﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadScreenManager : MonoBehaviour
{
    public void Respawn()
    {
        LocalPlayerManager.Instance.Respawn();
    }
    public void SwitchDeadScreen()
    {
        bool isActive = gameObject.activeSelf;

        gameObject.SetActive(!isActive);
    }
}
