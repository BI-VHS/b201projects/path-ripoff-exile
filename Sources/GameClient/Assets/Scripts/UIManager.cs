﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class UIManager : Singleton<UIManager>
{
    private MainMenuManager mainMenuManager;
    public MainMenuManager MainMenuManager
    {
        get{ return mainMenuManager; }
    }
    private InGameMenuManager inGameMenuManager;
    public InGameMenuManager InGameMenuManager
    {
        get{ return inGameMenuManager; }
    }
    private HudManager hudManager;
    public HudManager HudManager
    {
        get{ return hudManager; }
    }
    private ChatManager chatManager;
    public ChatManager ChatManager
    {
        get { return chatManager; }
    }

    private PlayerHealthbarManager healthbarManager;
    public PlayerHealthbarManager PlayerHealthbarManager
    {
        get { return healthbarManager; }
    }

    private LoadingScreenManager loadingScreenManager;
    public LoadingScreenManager LoadingScreenManager
    {
        get { return loadingScreenManager; }
    }
    private CursorManager cursorManager;
    public CursorManager CursorManager
    {
        get { return cursorManager; }
    }
    private MapManager mapManager;
    public MapManager MapManager
    {
        get { return mapManager; }
    }
    private DeadScreenManager deadScreenManager;
    public DeadScreenManager DeadScreenManager
    {
        get { return deadScreenManager; }
    }
    private QuickMenuManager quickMenuManager;
    public QuickMenuManager QuickMenuManagerr
    {
        get { return quickMenuManager; }
    }

    void Start()
    {
        mainMenuManager = GetComponentInChildren<MainMenuManager>();
        inGameMenuManager = GetComponentInChildren<InGameMenuManager>();
        hudManager = GetComponentInChildren<HudManager>();
        chatManager = GetComponentInChildren<ChatManager>();
        healthbarManager = GetComponentInChildren<PlayerHealthbarManager>();
        loadingScreenManager = GetComponentInChildren<LoadingScreenManager>();
        cursorManager = GetComponentInChildren<CursorManager>();
        mapManager = GetComponentInChildren<MapManager>();
        deadScreenManager = GetComponentInChildren<DeadScreenManager>();
        quickMenuManager = GetComponentInChildren<QuickMenuManager>();


        inGameMenuManager.gameObject.SetActive(false);
        hudManager.gameObject.SetActive(false);
        mapManager.gameObject.SetActive(false);
        deadScreenManager.gameObject.SetActive(false);
    }

    public void ChangeScene(Constants.SceneType _scene)
    {
        switch(_scene)
        {
            case Constants.SceneType.Devroom:
                SceneManager.LoadScene("Devroom");
                break;
            case Constants.SceneType.Mountains:
                SceneManager.LoadScene("MountainsTest");
                break;
            case Constants.SceneType.City:
                SceneManager.LoadScene("city2");
                break;
            case Constants.SceneType.Plains:
                SceneManager.LoadScene("Plains");
                break;
            case Constants.SceneType.Wasteland:
                SceneManager.LoadScene("WastelandTest");
                break;
            default:
                break;
        }
        // Not working correctly at the moment
        // Error : Trying to access destroyed component NavMeshAgent in MonsterManager -> After loading
        // Warning : Failed to create NavMeshAgent of the player -> Meanwhile loading
        
       // loadingScreenManager.LoadLevel(_scene);
    }
    public void ShowHud()
    {
        hudManager.gameObject.SetActive(true);
    }
    public void SwitchLoadingScreen()
    {
        bool isActive = loadingScreenManager.gameObject.activeSelf;

        loadingScreenManager.gameObject.SetActive(!isActive);
    }
    public void SwitchInGameMenu()
    {
        bool isActive = inGameMenuManager.gameObject.activeSelf;

        inGameMenuManager.gameObject.SetActive(!isActive);
    }
}
