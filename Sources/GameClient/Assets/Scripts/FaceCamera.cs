﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class FaceCamera : MonoBehaviour
{
    // Force object that is attached to this script to face the camera in every situation
    /*void Update()
    {
        Vector3 v = Camera.main.transform.position - this.transform.position;
        v.x = v.z = 0.0f;
        transform.LookAt(Camera.main.transform.position - v); 
        transform.Rotate(0,180,0);
    }*/
    void LateUpdate() 
    {
        transform.LookAt(transform.position + Camera.main.transform.forward);
    }
}
