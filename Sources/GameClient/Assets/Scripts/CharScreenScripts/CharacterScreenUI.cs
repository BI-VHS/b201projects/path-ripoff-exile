﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterScreenUI : MonoBehaviour
{
    public Transform slotParent;
    public GameObject charScreenUI;

    EquipSlot[] slots;

    // Start is called before the first frame update
    void Start()
    {
        // log the callback - update UI whe equipment changed 
        LocalPlayerManager.Instance.equip.onEquipmentChanged += UpdateUI;
        slots = slotParent.GetComponentsInChildren<EquipSlot>();
        charScreenUI.SetActive(false);
    }
    public void OpenCharacterWindow()
    {
        SoundManager.Instance.Play("WindowSound");
        charScreenUI.SetActive(!charScreenUI.activeSelf);
    }

    void UpdateUI( Equipment newItem, Equipment oldItem )
    {
        for (int i = 0; i < slots.Length; i++)
        {
            if (newItem != null && i == (int)newItem.equipSlot)
            {
                //equiped new item
                slots[i].AddItemToSlot(newItem);
                return;
            }

            if (oldItem != null && i == (int)oldItem.equipSlot)
            {
                //unequiped old item
                slots[i].ClearSlot();
                return;
            }
        }
    }
}
