﻿using UnityEngine;
using UnityEngine.UI;

public class ShowPlayerStats : MonoBehaviour
{
    public Text stats;
    public GameObject charScreenParent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (charScreenParent.activeSelf == true) 
        {
            string maxHealth = LocalPlayerManager.Instance.maxHealth.ToString();
            string currHealth = LocalPlayerManager.Instance.health.ToString();

            string maxMana = LocalPlayerManager.Instance.maxMana.ToString();
            string currMana = LocalPlayerManager.Instance.mana.ToString();

            int currArmor = LocalPlayerManager.Instance.basicArmorValue + LocalPlayerManager.Instance.armorModifiers;
            int currDamage = LocalPlayerManager.Instance.basicAttackDamage + LocalPlayerManager.Instance.damageModifiers;
            int currStrength = LocalPlayerManager.Instance.basicStrength + LocalPlayerManager.Instance.strengthModifiers;
            int currIntellect = LocalPlayerManager.Instance.basicIntellect + LocalPlayerManager.Instance.intellectModifiers;
            int currSpeed = LocalPlayerManager.Instance.basicSpeed + LocalPlayerManager.Instance.speedModifiers;

            int currExp = LocalPlayerManager.Instance.leveling.currentExperience;
            int needExp = LocalPlayerManager.Instance.leveling.neededExperience;

            stats.text = "Health: \n" + 
 	                     "\t" + currHealth + "/" + maxHealth + "\n" +
                         "Mana: \n" +
                         "\t" + currMana + "/" + maxMana + "\n" +
                         "Experience: \n" + 
                         "\t" + currExp + "/" + needExp + "\n" +
                         "\n" + 
                         "Armor:\t\t" + currArmor.ToString() + "\n" + 
                         "Damage:\t" + currDamage.ToString() + "\n" +
                         "\n" + 
                         "Strength:\t" + currStrength.ToString() + "\n" +
                         "Intellect:\t" + currIntellect.ToString() + "\n" + 
                         "Speed:\t\t" + currSpeed.ToString();


        }
    }
}
