﻿using UnityEngine;
using UnityEngine.UI;

public class ShowPlayerName : MonoBehaviour
{
    public Text playerName;
    public GameObject charScreenParent;

    // Start is called before the first frame update
    void Start()
    {
        playerName.text = LocalPlayerManager.Instance.username + "\n" + 
                          "LVL: " + LocalPlayerManager.Instance.leveling.playerLevel.ToString();
    }

    void Update()
    {
        if (charScreenParent.activeSelf == true) 
        {
            playerName.text = LocalPlayerManager.Instance.username + "\n" + 
                          "LVL: " + LocalPlayerManager.Instance.leveling.playerLevel.ToString();
        }
    }

}
