﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogueNodeJSON
{
    public DialogueNode[] nodes;

    public static DialogueNodeJSON CreateFromJSON(string jsonFileText)
    {
        return JsonUtility.FromJson<DialogueNodeJSON>(jsonFileText);
    }
}
