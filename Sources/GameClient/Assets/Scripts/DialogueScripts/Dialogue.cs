﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DialogueNPC", menuName = "Dialogue")]
public class Dialogue : ScriptableObject
{
    public TextAsset jsonFile;
    public string npcName;
}
