﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DialogueManager : Interactable
{
    public Dialogue dialogue;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI talkText;
    public GameObject dialogueUI;
    public GameObject continueButton;
    public GameObject choicesButton;

    private DialogueGraph dialogueGraph;
    private Dictionary<int,int> choices;
    private bool isActive;
    private bool isTyping;
    private int lastId;

    void Awake()
    {
        choices = new Dictionary<int, int>();
    }
    void Start()
    {
        dialogueGraph = new DialogueGraph(dialogue);
        dialogueUI.SetActive(false);

        isActive = false;
        isTyping = false;
        lastId = 0;
    }
    public override void Interact()
    {
        if(!isActive)
            StartDialogue();
    }
    public void CreateNewDialog(Dialogue newDialogue)
    {
        dialogue = newDialogue;
        dialogueGraph = new DialogueGraph(dialogue);
    }
    public bool GetIsActive() {return isActive;}
    void AddListeners()
    {
        // This makes sure the one button changes the listener in relation to object the player is interacting with at the moment
        continueButton.GetComponent<Button>().onClick.AddListener(delegate{Next();});

        choicesButton.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.AddListener(delegate{Choice(0);});
        choicesButton.transform.GetChild(1).gameObject.GetComponent<Button>().onClick.AddListener(delegate{Choice(1);});
        choicesButton.transform.GetChild(2).gameObject.GetComponent<Button>().onClick.AddListener(delegate{Choice(2);});
        choicesButton.transform.GetChild(3).gameObject.GetComponent<Button>().onClick.AddListener(delegate{Choice(3);});
    }
    void RemoveListeners()
    {
        continueButton.GetComponent<Button>().onClick.RemoveAllListeners();

        choicesButton.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
        choicesButton.transform.GetChild(1).gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
        choicesButton.transform.GetChild(2).gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
        choicesButton.transform.GetChild(3).gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
    }
    private void Next()
    {
        // If user clicks "too fast" through the dialogue, make sure to stop the coroutine of typing effect
        StopCoroutine("Type");
        isTyping = false;

        // Check if the dialogue is currently active (double checking, should not happen)
        if(isActive)
        {
            string textToType;
            int[] neighbours = GetNeighbours(lastId);
            
            // If next node has no neighbours, end the dialogue
            if(neighbours.Length == 0)
            {
                EndDialogue();
                return;
            }
            // There's only one neighbour, so progress to this node
            if (neighbours.Length == 1)
            {
                // Get the next node 
                DialogueNode nextNode = dialogueGraph.GetNode(neighbours[0]);
                
                // Get the player username
                if(nextNode.speaker == "player")
                    nextNode.speaker = LocalPlayerManager.Instance.username;

                textToType = nextNode.message;
                nameText.text = nextNode.speaker;
                lastId = nextNode.id;

                // The typing effect coroutine
                StartCoroutine("Type", textToType);
            }
            // There're multiple neighbours, so show the choices to the player
            if(neighbours.Length > 1)
                StartCoroutine("ShowPlayerChoices", neighbours);
        }
    }
    private void Choice(int idx)
    {
        // Get the chosen choice node id by pressed button index
        lastId = choices[idx];
        choices.Clear();

        foreach(Transform child in choicesButton.transform)
            child.gameObject.SetActive(false);

        choicesButton.SetActive(false);
        continueButton.SetActive(true);

        // Progress to the next node
        Next();
    }
    void StartDialogue()
    {
        LocalPlayerManager.Instance.navMeshAgent.isStopped = true;
        AddListeners();
        isActive = true;

        // Greeting message starts with id 0
        lastId = 0;
        DialogueNode node = dialogueGraph.GetNode(lastId);

        nameText.text = node.speaker;
        talkText.text = node.message;
        
        dialogueUI.SetActive(true);
        continueButton.SetActive(true);
        choicesButton.SetActive(false);

        foreach(Transform child in choicesButton.transform)
            child.gameObject.SetActive(false);
        
    }
    void EndDialogue()
    {
        choicesButton.SetActive(false);
        continueButton.SetActive(false);
        dialogueUI.SetActive(false);
        lastId = 0;
        
        isActive = false;
        RemoveListeners();
        LocalPlayerManager.Instance.navMeshAgent.isStopped = false;
    }
    int [] GetNeighbours(int id)
    {
        DialogueNode node = dialogueGraph.GetNode(id);
        return node.neighbours;
    }
    string GetText(int id)
    {
        DialogueNode node = dialogueGraph.GetNode(id);
        return node.message;
    }
    IEnumerator Type(string text)
    {
        isTyping = true;
        talkText.text = "";
       
        // Tiny delay between letters
        foreach (char c in text.ToCharArray())
        {
            talkText.text += c;
            yield return new WaitForSeconds(0.03f);
        }
        isTyping = false;
    }
    IEnumerator ShowPlayerChoices(int[] neighbours)
    {
        // This block makes sure player choices are shown after the typing effect is done
        while(isTyping)
            yield return null;
        
        // This also makes sure the text is displayed correctly
        DialogueNode nextNode = dialogueGraph.GetNode(lastId);
        talkText.text = nextNode.message;

        continueButton.SetActive(false);
        choicesButton.SetActive(true);

        // Save all neighbours as player choices and display them on buttons
        for(int i = 0; i < neighbours.Length; i++)
        {
            Transform child = choicesButton.transform.GetChild(i);

            child.gameObject.GetComponentInChildren<TextMeshProUGUI>().text = GetText(neighbours[i]);
            child.gameObject.SetActive(true);
            
            choices[i] = neighbours[i];
        }
    }
}