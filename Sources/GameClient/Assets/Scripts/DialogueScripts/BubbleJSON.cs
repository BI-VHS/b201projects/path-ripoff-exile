﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class for saving messages from JSON file in simple format
[System.Serializable]
public class BubbleJSON
{
    public string [] messages;

    public static BubbleJSON CreateFromJSON(string jsonFileText)
    {
        return JsonUtility.FromJson<BubbleJSON>(jsonFileText);
    }
}
