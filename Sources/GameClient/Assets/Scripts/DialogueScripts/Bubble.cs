﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bubble : MonoBehaviour
{
    private SpriteRenderer  background;
    private TextMeshPro     textMeshPro;
    public string           textDialogue;
    void Awake()
    {
        background = transform.Find("Background").GetComponent<SpriteRenderer>();
        textMeshPro = transform.Find("Text").GetComponent<TextMeshPro>();
    }
    // Setup the appropriate bubble length and text offset
    void Setup(string text)
    {
        textMeshPro.SetText(text);
        textMeshPro.ForceMeshUpdate();

        Vector2 textSize = textMeshPro.GetRenderedValues(false);

        Vector2 padding = new Vector2(7.0f, 2.0f);
        background.size = textSize + padding;

        Vector3 offset = new Vector3(-3.0f, 0.0f);
        background.transform.localPosition = new Vector3(background.size.x / 2.0f, 0.0f) + offset;
    }
    void Start()
    {   
        Setup(textDialogue);
        StartCoroutine("Type", textDialogue);
    }
    // The typing effect implemented using coroutine
    IEnumerator Type(string text)
    {
        textMeshPro.text = "";
       
        // Tiny delay between letters
        foreach (char c in text.ToCharArray())
        {
            textMeshPro.text += c;
            yield return new WaitForSeconds(0.05f);
        }
    }
    // Update is called once per frame
    void Update()
    {
    }
}
