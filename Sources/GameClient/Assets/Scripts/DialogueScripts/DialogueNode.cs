﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogueNode
{
    public int id;
    public string message;
    public string speaker;
    public int[] neighbours;
}
