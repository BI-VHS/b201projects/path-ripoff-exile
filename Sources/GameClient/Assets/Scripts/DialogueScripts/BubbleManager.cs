﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BubbleManager : MonoBehaviour
{
    public string           objectTag;
    public GameObject       chatBubble;
    public float            messageTime;
    public float            minSecondsSpawn;
    public float            maxSecondsSpawn;
    public TextAsset        jsonFile;

    private GameObject[]    characters;
    private List<string>    messages;
    private Vector3         offset;
    private Vector3         scale;
    private SortedSet<int>  activeNpc;
    private float           nextTime;
    void Awake()
    {
        messages = new List<string>();
        activeNpc = new SortedSet<int>();

        offset = new Vector3(-1.0f, 2.3f, 0.0f);
        scale = new Vector3(0.12f, 0.12f, 0.12f); 
        nextTime = 0.0f;
    }
    void Start()
    {
        // Get the JSON data of messages
        BubbleJSON dialogues = BubbleJSON.CreateFromJSON(jsonFile.text);
        
        // And add them to the list
        messages.AddRange(dialogues.messages);

        // Find objects with given tag
        if (characters == null)
            characters = GameObject.FindGameObjectsWithTag(objectTag);
    }
    IEnumerator SpawnMessage(int indexNpc, int indexMessage)
    {
        // This character is now actively displaying message
        activeNpc.Add(indexNpc);
        
        // Get the character position and instantiate the bubble with an appropiate offset
        Vector3 bubblePosition = characters[indexNpc].transform.position + offset;
        GameObject bubble = Instantiate(chatBubble, bubblePosition, Quaternion.identity);

        bubble.transform.localScale = scale;
        bubble.GetComponent<Bubble>().textDialogue = messages[indexMessage % messages.Count];
        
        float currentTime = Time.time;

        // Position the message along the character when it is changing its position
        while(Time.time - currentTime < messageTime)
        {
            bubble.transform.position = characters[indexNpc].transform.position + offset;
            yield return null;
        }
        // The character is now inactive and the message is destroyed
        activeNpc.Remove(indexNpc);
        Destroy(bubble);        
    }
    // Update is called once per frame
    void Update()
    {
        // This makes sure that the update function is executed randomly between min and max seconds
        float randomSpawn = Random.Range(minSecondsSpawn, maxSecondsSpawn);

        // Execute the code after ranomly chosen interval
        if (Time.time >= nextTime)
        {
            // Randomly choose which character and message will be displayed
            int randomNpc = Random.Range(0, characters.Length);
            int randomMessage = Random.Range(0, messages.Count);

            // If there's any other message that the character currently displays, choose again
            if (activeNpc.Contains(randomNpc))
                return;
            
            // Save the next random interval
            nextTime += randomSpawn;
            StartCoroutine(SpawnMessage(randomNpc, randomMessage));
        }
    }
}
