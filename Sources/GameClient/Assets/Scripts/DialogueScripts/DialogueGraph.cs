﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
 /*  This class makes a one-sided graph structure for storing the dialogue between NPC and player.
  */
public class DialogueGraph
{
    private Dialogue dialogue;
    private Dictionary<int, DialogueNode> nodes;

    public DialogueGraph (Dialogue addDialogue)
    {
        dialogue = addDialogue;
        nodes = new Dictionary<int, DialogueNode>();
        AddNodes();
    }
    private void AddNodes()
    {
        DialogueNodeJSON dialogueJSON = DialogueNodeJSON.CreateFromJSON(dialogue.jsonFile.text);

        foreach(DialogueNode node in dialogueJSON.nodes)
        {
            if(node.speaker != "player")
                node.speaker = dialogue.npcName;
            AddNode(node);
        }
    }
    private void AddNode(DialogueNode node)
    {
        nodes[node.id] = node;
    }
    public DialogueNode GetNode(int id)
    {
        DialogueNode node;

        if(nodes.TryGetValue(id, out node))
            return node;

        return null;
    }
}
