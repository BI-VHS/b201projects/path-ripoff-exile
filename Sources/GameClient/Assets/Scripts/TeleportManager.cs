﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class TeleportManager : Interactable
{
    public Constants.SceneType[] scenes;
    private Animator animator;
    private Button [] buttons;
    private bool isActive;

    void Start()
    {
        animator = gameObject.GetComponentInChildren<Animator>();
        buttons = gameObject.GetComponentsInChildren<Button>();

        foreach(Button b in buttons)
            b.gameObject.SetActive(false);

        for(int i = 0; i < scenes.Length; i++)
        {   
            buttons[i].gameObject.SetActive(true);

            int idx = i;

            buttons[i].onClick.AddListener(delegate{ChangeScene(scenes[idx]);});
            TextMeshProUGUI sceneText = buttons[i].GetComponentInChildren<TextMeshProUGUI>();
            sceneText.text = scenes[i].ToString();
        }
        isActive = false;   
    }
    public override void Interact()
    {
        isActive = true;
        animator.SetBool("isActive", isActive);
    }
    private void LateUpdate() 
    {
        if(!PlayerIsNearby() && isActive)
        {
            isActive = false;
            animator.SetBool("isActive", isActive);
        }
    }
    private bool PlayerIsNearby()
    {
        if (LocalPlayerManager.Instance != null)
        {
            float distance = Vector3.Distance(LocalPlayerManager.Instance.transform.position, transform.parent.position);

            if (distance < radius)
                return true;
        }
        return false;
    }
    private void ChangeScene(Constants.SceneType scene)
    {
        GameManager.Instance.ChangeScene(scene);
    }
}
