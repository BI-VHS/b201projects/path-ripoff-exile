﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//rand improts
using System;
using System.Threading;

public class GoblinMiner : MonoBehaviour
{
    public GameObject[] minePoints;
    public GameObject[] basePoints;
    private int destPoint = 0;
    private NavMeshAgent agent;
    public float viewRadius;
    public float attackRadius;
    public bool run = false;
    public bool attack = false;
    public uint workTime;
    public uint restTime;
    public float walkSpeed = 1.6f;
    public float runSpeed = 3.0f;
    public int initWaitTime;
    private Animator animator;
    private Transform lastPosition;
    private Transform playerPosition;


    private uint spotNumM;
    private uint spotNumR;
    private uint indexM;
    private uint indexR;

    private bool finishedM = true;
    private bool finishedR = true;
    private bool isWorking = false;
    private bool isAttacking = false;

    //bools
    private bool workRoute = false;
    private bool restRoute = false;
    private bool attackRoute = false;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        Thread.Sleep(1000 * initWaitTime);
        StartCoroutine("Work");
        agent.speed = walkSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (LocalPlayerManager.Instance != null) {
            playerPosition = LocalPlayerManager.Instance.transform;
        }


        if (!attack && !run && (workRoute || restRoute))
        {
            if (viewRadius > Vector3.Distance(playerPosition.position, gameObject.transform.position) && !isAttacking && !attackRoute)
            {
                run = true;
            }
        }

        if (!agent.pathPending && agent.remainingDistance < 0.5f && restRoute)
        {
            run = false;
            agent.speed = walkSpeed;
            StartCoroutine("Rest");
        }
        if (!agent.pathPending && agent.remainingDistance < 0.5f && workRoute)
        {
            StartCoroutine("Work");
        }
        if (attack)
        {
            if (!workRoute && !restRoute)
            {
                if (isWorking)
                {
                    finishedM = false;
                    lastPosition = minePoints[spotNumM].transform;
                    animator.SetBool("Work", false);
                }
                else
                {
                    finishedR = false;
                    lastPosition = basePoints[spotNumR].transform;
                    animator.SetBool("Rest", false);
                }
            }
            agent.updateRotation = true;
            agent.speed = runSpeed;
            StartCoroutine("AttackRun");
        }
        if (run)
        {

            if (!workRoute && !restRoute && isWorking)
            {
                finishedM = false;
                animator.SetBool("Work", false);
            }
            agent.updateRotation = true;
            agent.speed = runSpeed;
            StartCoroutine("RunHome");

        }

        if (attackRoute)
        {
            if (attackRadius < Vector3.Distance(transform.position, lastPosition.position))
            {
                attackRoute = false;
                isAttacking = false;
                animator.SetBool("Danger", false);
                animator.SetBool("Run", false);
                animator.SetBool("Walk", true);
                animator.SetBool("Attack", false);
                animator.SetBool("AttackRun", false);
                agent.updateRotation = true;
                agent.speed = walkSpeed;
                GoHome();
            }
            else if (!agent.pathPending && agent.remainingDistance < 1.0f)
            {
                agent.speed = walkSpeed;

                agent.updateRotation = false;
                Vector3 relativePos = playerPosition.position - transform.position;

                Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
                transform.rotation = rotation;
                agent.updateRotation = true;
                StartCoroutine("Attack");
            }
        }
        if (isAttacking && Vector3.Distance(playerPosition.position, gameObject.transform.position) > 1.0f)
        {
            agent.updateRotation = true;
            agent.speed = runSpeed;
            StartCoroutine("AttackRun");
        }

    }

    IEnumerator Work()
    {
        //finishedM = false;
        isWorking = true;
        agent.updateRotation = false;
        GoblinMinerWork mineSpotScript = minePoints[spotNumM].GetComponent<GoblinMinerWork>();
        Vector3 relativePos = mineSpotScript.stone.position - transform.position;

        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        transform.rotation = rotation;


        for (indexM = 0; indexM < mineSpotScript.goblins.Length; indexM++)
        {
            if (mineSpotScript.goblins[indexM] == null)
            {
                mineSpotScript.goblins[indexM] = gameObject;
                mineSpotScript.count++;
                break;
            }
        }

        workRoute = false;
        animator.SetBool("Work", true);
        animator.SetBool("Walk", false);
        for (int i = 0; i < workTime; i++)
        {
            int randomNum = UnityEngine.Random.Range(1, 3);
            for (int j = 0; j < randomNum; j++)
                if (!finishedM)
                    break;
            yield return new WaitForSeconds(11);
            if (!finishedM)
                break;
            animator.SetBool("Stretch", true);
            yield return new WaitForSeconds(9);
            animator.SetBool("Stretch", false);
        }

        if (finishedM)
        {
            animator.SetBool("Work", false);
            animator.SetBool("Walk", true);
            agent.updateRotation = true;

            mineSpotScript.goblins[indexM] = null;
            mineSpotScript.count--;

            finishedM = true;
            isWorking = false;

            GoHome();
        }
        finishedM = true;
    }
    IEnumerator Rest()
    {
        agent.updateRotation = false;
        GoblinMinerSave restSpotScript = basePoints[spotNumR].GetComponent<GoblinMinerSave>();
        Vector3 relativePos = restSpotScript.fire.position - transform.position;

        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        transform.rotation = rotation;


        for (indexR = 0; indexR < restSpotScript.goblins.Length; indexR++)
        {
            if (restSpotScript.goblins[indexR] == null)
            {
                restSpotScript.goblins[indexR] = gameObject;
                restSpotScript.count++;
                break;
            }
        }

        restRoute = false;
        animator.SetBool("Rest", true);
        animator.SetBool("Walk", false);
        animator.SetBool("Danger", false);
        animator.SetBool("Run", false);
        for (int i = 0; i < restTime; i++)
        {
            if (!finishedR)
                break;
            yield return new WaitForSeconds(10);
        }
        if (finishedR)
        {
            animator.SetBool("Rest", false);
            yield return new WaitForSeconds(3);
            animator.SetBool("Walk", true);
            agent.updateRotation = true;

            restSpotScript.goblins[indexR] = null;
            restSpotScript.count--;
            finishedR = true;
            GoToWork();
        }
        finishedR = true;
    }

    IEnumerator RunHome()
    {
        run = false;
        animator.SetBool("Danger", true);
        animator.SetBool("Work", false);
        animator.SetBool("Walk", false);
        yield return new WaitForSeconds(1);
        animator.SetBool("Run", true);
        GoHome();
    }

    IEnumerator AttackRun()
    {
        attack = false;
        animator.SetBool("Danger", true);
        animator.SetBool("Attack", false);
        animator.SetBool("Work", false);
        animator.SetBool("Walk", false);
        animator.SetBool("Rest", false);
        yield return new WaitForSeconds(1);
        animator.SetBool("AttackRun", true);
        GoToPlayer();
    }

    IEnumerator Attack()
    {
        isAttacking = true;
        attackRoute = false;
        animator.SetBool("AttackRun", false);
        animator.SetBool("Attack", true);
        yield return new WaitForSeconds(1);
        DoAttack();
    }

    void GoHome()
    {
        restRoute = true;
        agent.destination = basePoints[0].transform.position;
        spotNumR = 0;
    }

    void GoToWork()
    {
        workRoute = true;
        agent.destination = minePoints[0].transform.position;
        spotNumM = 0;
    }

    void GoToPlayer()
    {
        attackRoute = true;
        agent.destination = playerPosition.position;
    }

    void DoAttack()
    {
        print("Attacking");
        //TODO
    }
}
