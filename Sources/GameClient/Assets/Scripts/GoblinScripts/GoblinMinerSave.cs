﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GoblinMinerSave : MonoBehaviour
{
    public GameObject[] goblins;
    public uint count;
    public Transform fire;
    public float radius;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float dist = 100;
        if (LocalPlayerManager.Instance != null)
        {
            dist = Vector3.Distance(LocalPlayerManager.Instance.transform.position, gameObject.transform.position);

        }
        if (dist < radius)
        {
            foreach (GameObject goblin in goblins)
            {
                if (goblin != null)
                {
                    GoblinMiner goblinScript = goblin.GetComponent<GoblinMiner>();
                    goblinScript.attack = true;
                }
            }
            for (int i = 0; i < goblins.Length; i++)
            {
                goblins[i] = null;
            }
            count = 0;
        }
    }
}
