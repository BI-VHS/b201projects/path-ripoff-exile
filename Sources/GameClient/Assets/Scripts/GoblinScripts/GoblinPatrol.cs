﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GoblinPatrol : MonoBehaviour
{
    public Transform[] points;
    public Transform[] watchPoints;
    private int destPoint = 0;
    private NavMeshAgent agent;
    public float radius;
    public bool awared;
    private bool isWatching;
    public uint watchTime;
    private Transform playerPosition;

    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        animator.SetBool("Walk", true);
        GotoNextPoint();
    }

    // Update is called once per frame
    void Update()
    {
        if (LocalPlayerManager.Instance != null && Vector3.Distance(LocalPlayerManager.Instance.transform.position, gameObject.transform.position) <= radius)
        {
            awared = true;
            playerPosition = LocalPlayerManager.Instance.transform;
        }
        else
        {
            awared = false;
        }

        if (!agent.pathPending && agent.remainingDistance < 0.5f && !isWatching)
        {
            if (!awared)
            {
                StartCoroutine("Watch");
                GotoNextPoint();
            }
            else
                StartCoroutine("WatchPlayer");
        }
    }

    IEnumerator Watch()
    {
        isWatching = true;

        animator.SetBool("Walk", false);

        for (int i=0; i < watchPoints.Length; i++)
        {
            WatchNextPoint(i);
            yield return new WaitForSeconds(watchTime); 
        }
        isWatching = false;
        animator.SetBool("Walk", true);
    }

    IEnumerator WatchPlayer()
    {
        isWatching = true;
        animator.SetBool("Walk", false);
        while (awared)
        {
            WatchPlayerPosition();
            yield return new WaitForSeconds(1);
        }
        isWatching = false;
        animator.SetBool("Walk", true);
    }

    void GotoNextPoint()
    {
        // Returns if no points have been set up
        if (points.Length == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = points[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % points.Length;
    }

    void WatchNextPoint(int i)
    {
        agent.updateRotation = false;
        Vector3 relativePos = watchPoints[i].position - transform.position;

        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        transform.rotation = rotation;
        agent.updateRotation = true;
    }

    void WatchPlayerPosition()
    {
        agent.updateRotation = false;
        Vector3 relativePos = playerPosition.position - transform.position;

        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        transform.rotation = rotation;
        agent.updateRotation = true;
    }
}
