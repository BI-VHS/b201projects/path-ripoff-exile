﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

/* This class defines CLIENT-SIDE receiving of TCP packets.
 * This has to be the same, as in ServerSend class!!!
 */

public class ClientHandle : MonoBehaviour
{
    // Test welcome packet from the server, read and store values from obtained packet
    public static void Welcome(Packet _packet)
    {
        string _msg = _packet.ReadString();
        int _myId = _packet.ReadInt();
        

        Debug.Log($"Message from server: {_msg}");
        Client.Instance.myId = _myId;
        ClientSend.WelcomeReceived();

        // TCP connection done, initiate UDP
        Client.Instance.udp.Connect(((IPEndPoint)Client.Instance.tcp.socket.Client.LocalEndPoint).Port);
    }

    // CLIENT-SIDE receiving info about new player
    public static void SpawnPlayer(Packet _packet)
    {
        int id = _packet.ReadInt();
        string username = _packet.ReadString();
        Constants.CharacterClass characterClass = (Constants.CharacterClass) _packet.ReadInt();
        Vector3 position = _packet.ReadVector3();
        Quaternion rotation = _packet.ReadQuaternion();
        GameManager.Instance.SpawnPlayer(id, username, characterClass, position, rotation);
    }

    // One of the client from server was in our scene and left it. Delete his RemotePlayerManager Gameobject
    public static void PlayerLeftScene(Packet _packet)
    {
        int clientThatLeft = _packet.ReadInt();
        GameManager.Instance.DeleteRemotePlayer(clientThatLeft);
    }

    // One of the other clients on the server has moved and we obtained their id and position from server
    public static void PlayerMoved(Packet _packet)
    {
        int id = _packet.ReadInt();
        Vector3 newDestination = _packet.ReadVector3();
        GameManager.Instance.RemotePlayerMovement(id, newDestination);
    }

    // One of the monsters on the server has moved and we obtained their id and position from server
    public static void MonsterMoved(Packet _packet)
    {
        int id = _packet.ReadInt();
        Vector3 newDestination = _packet.ReadVector3();
        GameManager.Instance.MonsterMovement(id, newDestination);
    }

    // One of the other clients on the server has sent a message.
    // Add it to our local chat manager and register the message/
    public static void ReceiveChatMessage(Packet _packet)
    {
        string date = _packet.ReadString();
        string username = _packet.ReadString();
        string text = _packet.ReadString();

        // Add to local chat manager
        GameManager.Instance.RegisterChatMessage(new ChatMessage(date, username, text));
    }

    // Set the position of local player to obtained coordinates after he requested scene change
    public static void NewSceneSpawnPoint(Packet _packet)
    {
        Vector3 spawnPoint = _packet.ReadVector3();
        LocalPlayerManager.Instance.UpdatePosition(spawnPoint);
    }

    // Either received, when server spawned new monster or when we changed a scene
    // Obtained monster is currently in our scene
    public static void SpawnMonster(Packet _packet)
    {
        Constants.MonsterType monsterType = (Constants.MonsterType) _packet.ReadInt();
        int id = _packet.ReadInt();
        Vector3 position = _packet.ReadVector3();
        Quaternion rotation = _packet.ReadQuaternion();

        // Register monster in local GameManager
        GameManager.Instance.SpawnMonster(monsterType, id, position, rotation);
    }

    // Server informed us, that monster that is in our current scene took damage 
    // either from us or somebody else. Update our local values of monster
    public static void MonsterTookDamage(Packet _packet)
    {
        int id = _packet.ReadInt();
        int dealtDamage = _packet.ReadInt();

        // Find given monster via ID in our local representation
        GameManager.Instance.MonsterTookDamage(id, dealtDamage);
    }

    // Server informed us, that monster has died. Update local representation
    public static void MonsterDied(Packet _packet)
    {
        int monsterId = _packet.ReadInt();
        int playerIdThatKilledMonster = _packet.ReadInt();

        // Find given monster via ID in our local representation
        GameManager.Instance.MonsterDied(monsterId, playerIdThatKilledMonster);
    }

    // Server informed us, that one of other players in our scene has played a animation. Play it also for us.
    public static void PlayerPlayedAnimation(Packet _packet)
    {
        int playerId = _packet.ReadInt();
        string animation = _packet.ReadString();

        // Find given monster via ID in our local representation
        GameManager.Instance.PlayerPlayedAnimation(playerId, animation);
    }

    // Server informed us, that one of other players in our scene has played a animation. Play it also for us.
    public static void MonsterAttacked(Packet _packet)
    {
        int attackedPlayerId = _packet.ReadInt();
        int monsterId = _packet.ReadInt();
        int damageTaken = _packet.ReadInt();

        // Play monsters animation
        GameManager.Instance.MonsterAttacked(attackedPlayerId, monsterId, damageTaken);
    }

    // Server informed us, that we should warp to given location, do it
    public static void PlayerWarped(Packet _packet)
    {
        int playerId = _packet.ReadInt();
        Vector3 destination = _packet.ReadVector3();
        GameManager.Instance.PlayerWarped(playerId, destination);
    }

    // Delete disconnected player from our local copy of all players in dictionary
    public static void PlayerDisconnected(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Destroy(GameManager.players[_id].gameObject);
        GameManager.players.Remove(_id);
    }
}
