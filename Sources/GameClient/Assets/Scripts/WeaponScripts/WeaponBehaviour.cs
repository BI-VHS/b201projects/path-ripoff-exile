﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBehaviour : MonoBehaviour
{
    private AudioSource source;
    private Animator animator;
    private void Awake() 
    {
        animator = this.gameObject.transform.root.GetComponentInChildren<Animator>();
        source = this.gameObject.GetComponent<AudioSource>();
    }
    private void OnTriggerEnter(Collider other)
    {        
        if(other.gameObject.CompareTag("Monster") && animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            PlayHitSound();
    }
    private void PlayHitSound()
    {
        source.Play();
    }
}
