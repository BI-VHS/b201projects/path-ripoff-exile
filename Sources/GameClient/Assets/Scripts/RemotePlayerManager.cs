﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RemotePlayerManager : MonoBehaviour
{
    public int id;
    public string username;
    public Animator animator;
    public NavMeshAgent navMeshAgent;
    public PlayerAbilityManager abilities;

    // Start is called before the first frame update
    private void Start()
    {
        animator = GetComponentInChildren<Animator>();
        abilities = GetComponentInChildren<PlayerAbilityManager>();
    }
    void Update()
    {
       /* if (animator.GetCurrentAnimatorStateInfo(0).IsName("Ability0"))
        {
            //particles1.Play();
            //print("doing remote particless");
        }*/
        if(!animator.GetCurrentAnimatorStateInfo(0).IsName("Ability0"))
            abilities.StopAbility(0);

        // Switch back to idle if not walking
        if (navMeshAgent.destination == transform.position)
        {
            animator.SetBool("isWalking", false);
        }
    }

    // Updates the target destination for nav mesh agent
    public void UpdateDestination(Vector3 _newDestination)
    {
        navMeshAgent.destination = _newDestination;
        animator.SetBool("isWalking", true);
    }

    public void UpdatePosition(Vector3 _newPosition)
    {
        navMeshAgent.Warp(_newPosition);
    }
}
