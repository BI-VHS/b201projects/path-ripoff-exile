﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class KnightPatrol : MonoBehaviour
{
    public string           objectTag;
    public float            radius; 
    public float            talkTime;
    public Transform[]      points;
    private int             destPoint = 0;
    private NavMeshAgent    agent;
    private GameObject[]    patrolKnights;
    private GameObject      lastKnight;
    private Animator        animator;
    private float           delay = 3.0f;
    [HideInInspector]
    public float            lastTalk;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        
        lastTalk = float.MinValue;
        agent.autoBraking = true;

        // Make the patrol talking routine with objects under this tag
        if (patrolKnights == null)
            patrolKnights = GameObject.FindGameObjectsWithTag(objectTag);

        GotoNextPoint();
    }

    void Update()
    {
        if (agent.isStopped == false)
        {
            agent.updateRotation = true;

            if(!agent.pathPending && agent.remainingDistance < 0.5f)
                GotoNextPoint();

            // If is ready to talk
            if (Time.time - lastTalk >= talkTime*delay)
            {
                foreach (GameObject knight in patrolKnights)
                {   
                    // Skip myself
                    if(knight.transform == transform)
                        continue;

                    // Get the distance between myself and the other knight
                    float distance = Vector3.Distance(knight.transform.position,transform.position);

                    if(distance <= radius)
                    {
                        // Save the other knight object and start talking
                        lastKnight = knight;
                        StartCoroutine("Talk");
                        break;
                    }
                }
            }
        }
        // Else the knights are talking, so update the rotation
        else if (lastKnight != null)
        {
            agent.updateRotation = false;

            Vector3 forward = lastKnight.transform.position - transform.position;

            // Face the knights
            Quaternion lookAt = Quaternion.LookRotation(forward);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookAt,Time.deltaTime*5.0f);
        }
        
        animator.SetBool("isTalking", agent.isStopped);
    }
    IEnumerator Talk()
    {
        lastTalk = Time.time;
        agent.isStopped = true;

        // Wait and do nothing while knights are talking
        yield return new WaitForSeconds(talkTime);
        agent.isStopped = false;
    }
    void GotoNextPoint()
    {
        if (points.Length == 0)
            return;

        // Cycle to the next point
        agent.destination = points[destPoint].position;
        destPoint = (destPoint + 1) % points.Length;
    }
}
