﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ChatManager : MonoBehaviour
{
    public int maxMessages = 20;
    public GameObject chatPanel;
    public GameObject messagePrefab;
    public InputField inputField;
    private List<ChatMessage> messages;

    private void Start()
    {
        messages = new List<ChatMessage>();

        // Check to see, if all in-editor dependecies are correctly setup
        Assert.IsNotNull(chatPanel);
        Assert.IsNotNull(messagePrefab);
        Assert.IsNotNull(inputField);
    }
    public bool InputFieldFocused()
    {
        return inputField.isFocused;
    }
    public void SendMessage(string _username, string _message)
    {
        ChatMessage message = new ChatMessage(DateTime.Now.ToString("HH:mm"), _username, _message);
        RegisterChatMessage(message);
    }
    public void SendItemMessage(string _username, string _itemName)
    {
        string itemText = "Picked up " + _itemName + "";
        ChatMessage message = new ChatMessage(DateTime.Now.ToString("HH:mm"), _username, itemText);
        RegisterChatMessage(message);
    }
    // Checks if input bot is not empty and then sends out the message from user to all others via server
    public void SendChatMessage(string _username)
    {
        if(inputField.text != "")
        {
            ChatMessage message = new ChatMessage(DateTime.Now.ToString("HH:mm"), _username, inputField.text);
            inputField.text = "";

            RegisterChatMessage(message);

            // Send to to all other clients via server
            ClientSend.SendChatMessage(message);
        }
    }

    public void RegisterChatMessage(ChatMessage _message)
    {
        // Keep only few of the latest messages
        if(messages.Count >= maxMessages)
        {
            Destroy(chatPanel.gameObject.transform.GetChild(0).gameObject);
            messages.Remove(messages[0]);
        }
        messages.Add(_message);

        // Display the message in the HUD
        GameObject hudMessage = Instantiate(messagePrefab, chatPanel.transform);

        // Display the message correctly and formatted
        hudMessage.GetComponent<Text>().text = "[" + _message.Date + "] " + _message.Username + ": " + _message.Text;
        inputField.ActivateInputField();
        inputField.Select();
    }
}
