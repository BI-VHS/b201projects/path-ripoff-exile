﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuNPC : MonoBehaviour
{
    public GameObject info;
    public Vector3 enlargement = new Vector3(0.2f, 0.2f, 0.2f);
    public Constants.CharacterClass characterClass;
    private bool enlarged = false;
    private Animator animator;
    private string currentAnimaton = "Sitting";

    void Start()
    {
        animator = GetComponent<Animator>();
        info.SetActive(false);
    }

    void Update()
    {
        // User selected some other character class then us
        if(GameManager.Instance.chosenCharacterClass != Constants.CharacterClass.None && GameManager.Instance.chosenCharacterClass != characterClass)
        {
            ChangeAnimationState("Disbelief");
        }
    }

    void OnMouseOver()
    {
        if(!enlarged && GameManager.Instance.chosenCharacterClass != characterClass)
        {
            //transform.localScale += enlargement;
            ChangeAnimationState("Standing");
            enlarged = true;
        }
        info.SetActive(true);
    }

    void OnMouseDown()
    {
        ChangeAnimationState("Joy");
        GameManager.Instance.chosenCharacterClass = characterClass;
    }

    void OnMouseExit()
    {
        if(enlarged && GameManager.Instance.chosenCharacterClass != characterClass)
        {
            //transform.localScale -= enlargement;
            ChangeAnimationState("Sitting");
            enlarged = false;
        }
        info.SetActive(false);
    }

    void ChangeAnimationState(string newAnimation)
    {
        if (currentAnimaton == newAnimation) return;

        animator.Play(newAnimation);
        currentAnimaton = newAnimation;
    }

}
