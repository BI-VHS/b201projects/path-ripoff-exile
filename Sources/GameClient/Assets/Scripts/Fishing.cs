﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Fishing : MonoBehaviour
{
    public Transform[] points;
    public string[] pointAction;
    public string weaponName;
    public float fishingTime;
    public float sellingTime;
    public float shopTime;

    private int destPoint = 0;
    private NavMeshAgent agent;
    private Animator animator;
    private GameObject weapon;

    [HideInInspector]
    public bool isSelling = true;
    [HideInInspector]
    public bool isFishing = false;
    [HideInInspector]
    public bool isMakingShop = false;


    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        agent.autoBraking = true;

        Transform findWeapon = FindChildRecursively(transform, weaponName);
        
        if(findWeapon != null)
        {
            weapon = findWeapon.gameObject;
            weapon.SetActive(false);
        }
        else
            Debug.Log("null");
    }

    Transform FindChildRecursively(Transform parent, string name)
    {
        Transform findChild = parent.Find(name);

        if(findChild != null)
            return findChild;

        foreach(Transform child in parent)
        {
            Transform findGrandChild = FindChildRecursively(child, name);

            if(findGrandChild != null)
                return findGrandChild;
        }
        return null;
    }
    void Update()
    {
        if(agent.isStopped == false)
        {
            agent.updateRotation = true;

            if(!agent.pathPending && AgentIsClose())
                GotoNextPoint();

            if (pointAction[destPoint] == "Selling" && AgentIsClose())
                StartCoroutine("Sell");

            else if (pointAction[destPoint] == "Fishing" && AgentIsClose())
                StartCoroutine("Fish");

            else if (pointAction[destPoint] == "Making shop" && AgentIsClose())
                StartCoroutine("Shop");
        }
        else if (isSelling)
        {
            Vector3 forward = new Vector3(-0.5f, 0.0f, -1.0f);
            Rotate(forward);
        }

        animator.SetBool("isSelling", isSelling);
        animator.SetBool("isMakingShop",isMakingShop);
        animator.SetBool("isFishing",isFishing);
        animator.SetBool("isWalking", !agent.isStopped);
    }
    void Rotate(Vector3 forward)
    {
        agent.updateRotation = false;
        Quaternion lookAt = Quaternion.LookRotation(forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookAt,Time.deltaTime*5.0f);
    }
    bool AgentIsClose()
    {
        return agent.remainingDistance < 0.5f;
    }
    IEnumerator Sell()
    {
        isSelling = true;
        agent.isStopped = true;

        yield return new WaitForSeconds(sellingTime);
        agent.isStopped = false;
        isSelling = false;
    }
    IEnumerator Fish()
    {
        isFishing = true;
        agent.isStopped = true;
        weapon.SetActive(true);

        yield return new WaitForSeconds(fishingTime);
        weapon.SetActive(false);
        agent.isStopped = false;
        isFishing = false;
    }
    IEnumerator Shop()
    {
        isMakingShop = true;
        agent.isStopped = true;

        yield return new WaitForSeconds(shopTime);
        agent.isStopped = false;
        isMakingShop = false;
    }
    void GotoNextPoint()
    {
        if (points.Length == 0)
            return;

        agent.destination = points[destPoint].position;
        destPoint = (destPoint + 1) % points.Length;
    }
}
