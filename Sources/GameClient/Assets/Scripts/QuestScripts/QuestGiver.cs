﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestGiver : MonoBehaviour
{
    public Quest quest;
    //public GameObject characterParent;
    public Dialogue questActiveDialogue;
    public Dialogue questFinishedDialogue;
    public GameObject questPopUpIcon;
    public bool questAssigned = false;
    private DialogueManager dialogueManager;

    void Start()
    {
        dialogueManager = gameObject.GetComponentInChildren<DialogueManager>();
    }

    void Update()
    {
        if (LocalPlayerManager.Instance != null) 
        {
            if (LocalPlayerManager.Instance.questing != null)
            {
                if (LocalPlayerManager.Instance.questing.quests != null)
                {
                    // quest active, but unfinished
                    if (LocalPlayerManager.Instance.questing.quests.Contains(quest))
                    {
                        if (!dialogueManager.GetIsActive()) 
                        {
                            SetDialog(questActiveDialogue);
                            return;
                        }
                    }
                    // quest finished
                    else if (LocalPlayerManager.Instance.questing.finishedQuests.Contains(quest))
                    {
                        if (!dialogueManager.GetIsActive())
                        {
                            SetDialog(questFinishedDialogue);
                            questPopUpIcon.SetActive(false);
                            return;
                        }
                    }
                }
            }
        }
    }

    public void SetDialog(Dialogue newDialogue)
    {
        dialogueManager.CreateNewDialog(newDialogue);
    }

}
