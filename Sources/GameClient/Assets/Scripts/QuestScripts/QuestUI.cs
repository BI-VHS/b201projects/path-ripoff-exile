﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestUI : MonoBehaviour
{
    public GameObject questWindow;
    public GameObject questParent;
    public Text titleText;
    public Text descriptionText;
    public Text progressText;
    public GameObject claimButton;
    public GameObject prevButton;
    public GameObject nextButton;
    public GameObject itemRewardParent;
    public Image itemIcon;
    public Text expText;
    public Text goldText;
    public Text itemText;

    Quest quest;

    // Start is called before the first frame update
    void Start()
    {
        LocalPlayerManager.Instance.questing.onQuestChanged += UpdateUI;
        questWindow.SetActive(false);
    }
    public void OpenQuestWindow()
    {
        questWindow.SetActive(!questWindow.activeSelf);
        SoundManager.Instance.Play("WindowSound");

        if (LocalPlayerManager.Instance.questing.quests.Count != 0)
            questParent.SetActive(true);
        else
        {
            questParent.SetActive(false);
            titleText.text = "No active quest";
        }
    }

    
    public static void Swap<T>(IList<T> list, int indexA, int indexB)
    {
        T tmp = list[indexA];
        list[indexA] = list[indexB];
        list[indexB] = tmp;
    }

    public void ShowNextQuest()
    {
        for (int i = 0; i < LocalPlayerManager.Instance.questing.quests.Count-1; i++)
        {
            Swap(LocalPlayerManager.Instance.questing.quests, i, i+1 );
        }
        UpdateUI();
        SoundManager.Instance.Play("WindowSound");
    }

    public void ShowPreviousQuest()
    {
        for (int i = LocalPlayerManager.Instance.questing.quests.Count-1; i > 0; i--)
        {
            Swap(LocalPlayerManager.Instance.questing.quests, i, i-1 );
        }
        UpdateUI();
        SoundManager.Instance.Play("WindowSound");
    }

    public void UpdateUI()
    {
        if (LocalPlayerManager.Instance.questing.quests.Count != 0)
        {
            quest = LocalPlayerManager.Instance.questing.quests[0];
            questParent.SetActive(true);
            titleText.text = quest.title;
            descriptionText.text = quest.description;
            expText.text = "EXP: " + quest.expReward.ToString();
            goldText.text = "GOLD: " + quest.goldReward.ToString();
            //item reward
            if (quest.itemReward != null)
            {
                itemRewardParent.SetActive(true);
                itemIcon.sprite = quest.itemReward.icon;
                itemText.text = "";

                if(quest.itemReward is Equipment)
                {
                    if (((Equipment)quest.itemReward).armorModifier != 0)
                        itemText.text = "ARM +" + ((Equipment)quest.itemReward).armorModifier.ToString() + "\n";
                    if (((Equipment)quest.itemReward).damageModifier != 0)
                        itemText.text += "DMG +" + ((Equipment)quest.itemReward).damageModifier.ToString();
                }
            }
            else
            {
                itemRewardParent.SetActive(false);
                itemText.text = "";
            }
            // quest switching
            if (LocalPlayerManager.Instance.questing.quests.Count > 1)
            {
                prevButton.SetActive(true);
                nextButton.SetActive(true);
            }
            else
            {
                prevButton.SetActive(false);
                nextButton.SetActive(false);
            }
            // claim button
            if (!quest.finished)
            {
                progressText.text = "Quest Progress:\n" + quest.currentAmount.ToString() + "/" + quest.requiredAmount.ToString();
                claimButton.SetActive(false);
            }
            else
            {
                progressText.text = "Quest Progress:\nCOMPLETE";
                claimButton.SetActive(true);
            }
        }
        else
        {
            questParent.SetActive(false);
            titleText.text = "No active quest";
        }
    }

    public void ClaimReward()
    {
        quest.FinishQuest();
        SoundManager.Instance.Play("QuestSound");
    }
}
