﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    public delegate void OnQuestChanged();
    public OnQuestChanged onQuestChanged;

    public List<Quest> quests;
    public List<Quest> finishedQuests;


    // Start is called before the first frame update
    void Start()
    {
        quests = new List<Quest>();
        finishedQuests = new List<Quest>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddQuest(Quest quest)
    {
        quests.Add(quest);
        // Check if we already have the items to complete the quest
        for (int i = 0; i < LocalPlayerManager.Instance.inventory.items.Count; i++)
        {
            for (int j = 0; j < LocalPlayerManager.Instance.questing.quests.Count; j++)
            {
                LocalPlayerManager.Instance.questing.quests[j].ProgressMade(LocalPlayerManager.Instance.inventory.items[i]);

                if (LocalPlayerManager.Instance.questing.quests[j].IsReached())
                    LocalPlayerManager.Instance.questing.quests[j].finished = true;
            }
        }
        UpdateQuestWindow();
    }

    public void UpdateQuestWindow()
    {
        if (onQuestChanged != null)
            onQuestChanged.Invoke();
    }

}
