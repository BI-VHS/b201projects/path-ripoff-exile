﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Kill Quest", menuName = "Quests/Kill quest")]
public class KillQuest : Quest
{
    public Constants.MonsterType monsterType;
    
    public override void ProgressMade(Constants.MonsterType killedType)
    {
        if (monsterType == killedType)
            currentAmount++;
        return;
    }

    public override void ProgressMade(Item itemPicked) 
    {
        return;
    }
}
