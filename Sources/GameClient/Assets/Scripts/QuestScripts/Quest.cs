﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest : ScriptableObject
{
    public bool isActive;
    public string title;
    public string description;
    
    public int expReward;
    public int goldReward;
    public Item itemReward;
    
    public int requiredAmount;
    public int currentAmount;

    //public Equipment equipReward;

    public bool finished;
    
    public bool IsReached()
    {
        return (currentAmount >= requiredAmount);
    }

    public virtual void FinishQuest()
    {
        //claim reward
        if (LocalPlayerManager.Instance.questing.quests[0].itemReward != null)
        {
            bool enoughSpace = LocalPlayerManager.Instance.inventory.Add(LocalPlayerManager.Instance.questing.quests[0].itemReward);
            if (!enoughSpace)
                return;
            
            if(itemReward is QuestItem)
            {
                for (int j = 0; j < LocalPlayerManager.Instance.questing.quests.Count; j++)
                {
                    LocalPlayerManager.Instance.questing.quests[j].ProgressMade(itemReward);

                    if (LocalPlayerManager.Instance.questing.quests[j].IsReached())
                        LocalPlayerManager.Instance.questing.quests[j].finished = true;

                    LocalPlayerManager.Instance.questing.UpdateQuestWindow();
                }
            }
        }
        LocalPlayerManager.Instance.leveling.currentExperience += LocalPlayerManager.Instance.questing.quests[0].expReward;
        LocalPlayerManager.Instance.inventory.gold += LocalPlayerManager.Instance.questing.quests[0].goldReward;

        //remove the quest
        LocalPlayerManager.Instance.questing.quests[0].currentAmount = 0;
        LocalPlayerManager.Instance.questing.quests[0].finished = false;
        LocalPlayerManager.Instance.questing.finishedQuests.Add(LocalPlayerManager.Instance.questing.quests[0]);
        LocalPlayerManager.Instance.questing.quests.RemoveAt(0);
        LocalPlayerManager.Instance.questing.UpdateQuestWindow();
    }

    public virtual void ProgressMade(Constants.MonsterType killedType) {}
    public virtual void ProgressMade(Item itemPicked) {}

}
