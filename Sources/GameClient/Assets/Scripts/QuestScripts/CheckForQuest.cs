﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CheckForQuest : MonoBehaviour
{
    public TextMeshProUGUI choiceText;

    public void QuestCheck()
    {
        if (choiceText.text == "QUEST") 
        {
            Interactable person = LocalPlayerManager.Instance.focus;
            QuestGiver questGiver = person.GetComponentInChildren<QuestGiver>();
            Quest quest = questGiver.quest;

            if (questGiver.questAssigned == false)
            {
                LocalPlayerManager.Instance.questing.AddQuest(quest);
                questGiver.questAssigned = true;
                
                print("QUEST " + quest.title + " ADDED");
            }
        }
    }
}
