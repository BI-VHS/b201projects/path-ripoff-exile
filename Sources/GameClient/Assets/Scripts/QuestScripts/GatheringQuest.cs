﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Gather Quest", menuName = "Quests/Gathering quest")]
public class GatheringQuest : Quest
{
    public Item questItem;
    public override void ProgressMade(Item itemPicked) 
    {
        if (itemPicked == questItem)
            currentAmount++;
        return;
    }

    public override void ProgressMade(Constants.MonsterType killedType) {return;}
    public override void FinishQuest()
    {
        base.FinishQuest();
        
        //delete the quest items
        for (int i = LocalPlayerManager.Instance.inventory.items.Count-1; i >= 0; i--)
        {
            if (LocalPlayerManager.Instance.inventory.items[i] == questItem)
                LocalPlayerManager.Instance.inventory.RemoveQuestItem(questItem);

        }
    }
}
