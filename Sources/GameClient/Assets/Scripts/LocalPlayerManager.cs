﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.Assertions;

public class LocalPlayerManager : Singleton<LocalPlayerManager>
{
    public int id;
    public string username;
    private Animator animator;
    private AudioSource source;
    public NavMeshAgent navMeshAgent;
    public Interactable focus;
    public Inventory inventory; 
    public EquipmentManager equip;
    public LevelingManager leveling;
    public QuestManager questing;
    public PlayerSoundManager sound;
    public PlayerAbilityManager abilities;
    public float attackRange;
    public GameObject attackTarget;
    public Constants.CharacterClass characterClass;

    // Stats, might needs own class
    public int health;
    public int maxHealth = 500;
    public int mana;
    public int maxMana = 100;
    public int basicAttackDamage = 10;
    public int damageModifiers = 0;
    public int basicArmorValue = 5;
    public int armorModifiers = 0;
    public int basicStrength = 2;
    public int strengthModifiers = 0;
    public int basicIntellect = 2;
    public int intellectModifiers = 0;
    public int basicSpeed = 2;
    public int speedModifiers = 0;
    private int currentAbility = -1;
    //times
    private float manaTime = 0.0f; //last time when was mana restored
    public bool alive = true;

    private void Start()
    {
        Assert.IsNotNull(navMeshAgent);
        animator = GetComponentInChildren<Animator>();
        source = GetComponentInChildren<AudioSource>();
        inventory = GetComponentInChildren<Inventory>();
        equip = GetComponentInChildren<EquipmentManager>();
        leveling = GetComponentInChildren<LevelingManager>();
        questing= GetComponentInChildren<QuestManager>();
        sound = GetComponentInChildren<PlayerSoundManager>();
        abilities = GetComponentInChildren<PlayerAbilityManager>();

        // log the callback - update stats when equipment changed 
        equip.onEquipmentChanged += UpdateStats;

        UIManager.Instance.PlayerHealthbarManager.SetIcon(characterClass);
        UIManager.Instance.PlayerHealthbarManager.SetMaxHealth(maxHealth);
        UIManager.Instance.PlayerHealthbarManager.SetMaxMana(maxMana);

        HandlePlayerHealthbar(maxHealth, maxMana);
    }

    private void Update()
    {
        if(!alive)
        {
            HandleChat();
            return;
        }
        // if (Input.GetKey("2"))
        // {
        //     leveling.currentExperience += 30;
        //     inventory.gold += 30;
        // }
        if (Input.GetKey("1") && mana > abilities.GetAbility(0).cost)
        {
            if(Time.time - abilities.GetAbility(0).time > 1.0f)    
                HandleAbility(0);
        }
        else
        {
            if (currentAbility > -1)
            {
                //saying you no longer doing any ability
                currentAbility = -1;

                animator.Play("Idle");
                ClientSend.PlayAnimation("Idle");

                abilities.StopAbility(0);
            }

            HandleMovement();
            HandleChat();
            HandleInGameMenu();
            HandleBasicAttack();
            HandleRestoreMana();
        }

        // Switch back to idle if not walking
        if(navMeshAgent.destination == transform.position)
        {
            animator.SetBool("isWalking", false);
        }
    }

    // Changes the destination point of navigation of player upon mouse click
    private void HandleMovement()
    {
        // User clicked on some UI element, don't move
        if(EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        // Cast a ray a get new position where to move
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        // Mouse right click
        if(Input.GetMouseButtonDown(1))
        {
            if(Physics.Raycast(ray, out hit, 100))
            {
                UpdateDestination(hit.point);

                animator.SetBool("isWalking", true);
                
                // Inform the server about new destination
                ClientSend.PlayerMovement(hit.point);

                // User clicked on monster, that was not previously clicked
                if(hit.collider.gameObject.CompareTag("Monster"))
                {
                    attackTarget = hit.collider.gameObject;
                }
                else
                {
                    attackTarget = null;
                }

                // if clicked on an item, enemy, etc. -> interact with it
                Interactable interactable = hit.collider.GetComponent<Interactable>();
                if(interactable != null)
                {
                    if(focus != interactable)
                    {
                        print("Setting focus on " + interactable);
                        SetFocus(interactable);
                        UIManager.Instance.CursorManager.ActivateHandHoverCursor();
                    }
                }
                else
                {
                    RemoveFocus();
                }
            }
        }
    }

    // If user presses 'enter' send a new message to chat
    private void HandleChat()
    {
        if(Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            GameManager.Instance.SendChatMessage(this.username);
        }
    }

    // If user presses 'esc' inform UIManager that it should bring up in game menu
    private void HandleInGameMenu()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            UIManager.Instance.SwitchInGameMenu();
        }
    }
    // Checks to see if any valid attack target is set and player is close enough to attack it
    // Player is attacking via basic auto attack (right click on enemy)
    private void HandleBasicAttack()
    {
        // Verify, that we have clicked on something with tag Monster and it has MonsterManager attach to it
        if(attackTarget != null && attackTarget.GetComponent<MonsterManager>() != null)
        {
            if(Vector3.Distance(transform.position, attackTarget.transform.position) <= attackRange)
            {
                // Play animation
                animator.Play("Attack");

                // Play animation for all other clients via server
                ClientSend.PlayAnimation("Attack");

                // Calculate dealt damage, 1 STR point means +5% damage
                int damage = (int)( ((float)basicAttackDamage + (float)damageModifiers) * (1.0 + ((float)basicStrength*5.0/100.0)) );
                print("Dealed dmg: " + damage.ToString());

                // Get monster's id from monster manager attached to attacked target
                int monsterId = attackTarget.GetComponent<MonsterManager>().id;

                // Send info about the attack and dealt damage to server, where it monster's health is adjusted
                // and other Clients are informed
                ClientSend.DealDamageToMonster(monsterId, damage);

                // Reset target after one attack is done
                attackTarget = null;
            }
        }
    }
    private void HandleAbility(int ability)
    {
        abilities.GetAbility(ability).time = Time.time;

        // if you wasnt doing ability start doing it
        if (currentAbility != ability)
        {
            currentAbility = ability;

            animator.Play("Ability" + ability.ToString());
            ClientSend.PlayAnimation("Ability" + ability.ToString());
        }
         //pro Maru bylo by lepsi udelat na serveru nejakou funkci co umi menit animator bool Ability pro remote playera, bude to vic smooth

        //Stop the movement
        UpdateDestination(transform.position);
        ClientSend.PlayerMovement(transform.position);

        UpdateMana(-abilities.GetAbility(ability).cost);

        /*switch (characterClass)
        {
            case Constants.CharacterClass.Warrior:
                //do warrior ability
                //taunt enemies and higher up defense
                break;
            case Constants.CharacterClass.Mage:
                //do mage ability
                break;
            case Constants.CharacterClass.Thief:
                //do thief ability
                break;
            case Constants.CharacterClass.Medic:
                //do medic ability
                break;
            default:
                break;
        }*/
    }

    private void HandleRestoreMana()
    {
        if (mana < maxMana && Time.time - manaTime >= 10.0f)
        {
            print("Mana restored!! current mana is now " + mana);
            manaTime = Time.time;
            
            int restoredMana = (int)( ((float)intellectModifiers) + ((float)basicIntellect));

            UpdateMana(restoredMana);
        }
        else if (mana > maxMana)
            mana = maxMana;
    }
    public void HandlePlayerHealthbar(int newHealth, int newMana)
    {
        health = newHealth;
        mana = newMana;

        UIManager.Instance.PlayerHealthbarManager.SetHealth(newHealth);
        UIManager.Instance.PlayerHealthbarManager.SetMana(newMana);
    }
    public void UpdateMana(int _mana)
    {
        mana += _mana;

        mana = Mathf.Clamp(mana, 0, maxMana);
        UIManager.Instance.PlayerHealthbarManager.SetMana(mana);
    }
    public void UpdateHealth(int _health)
    {
        health += _health;

        health = Mathf.Clamp(health, 0, maxHealth);
        UIManager.Instance.PlayerHealthbarManager.SetHealth(health);
    }
    // Updates the target destination for nav mesh agent
    public void UpdateDestination(Vector3 _newDestination)
    {
        navMeshAgent.destination = _newDestination;
    }
    public void UpdatePosition(Vector3 _newPosition)
    {
        navMeshAgent.Warp(_newPosition);
    }
    // update player stats if equipment changed
    public void UpdateStats( Equipment newItem, Equipment oldItem )
    {
        if (newItem != null && oldItem == null)
        {
            //equiped new item
            armorModifiers += newItem.armorModifier;
            damageModifiers += newItem.damageModifier;
            return;
        }

        if (newItem != null && oldItem != null)
        {
            //switched items
            armorModifiers += newItem.armorModifier;
            damageModifiers += newItem.damageModifier;

            armorModifiers -= oldItem.armorModifier;
            damageModifiers -= oldItem.damageModifier;
            return;
        }

        if (newItem == null && oldItem != null)
        {
            //unequiped old item
            armorModifiers -= oldItem.armorModifier;
            damageModifiers -= oldItem.damageModifier;
            return;
        }
    }
    public void TakeDamage(int _damage)
    {
        // Monsters should check for player alive status before attacking
        int damage = (int)( ((float)_damage - (float)armorModifiers) * (1.0 + ((float)basicArmorValue*5.0/100.0)) );

        UpdateHealth(-damage);

        if(health <= 0 && alive)
            Die();
    }
    public void Die()
    {
        alive = false;
        
        animator.Play("Die");
        ClientSend.PlayAnimation("Die");
        
        int loseXP = (int)((float)(leveling.currentExperience*5.0/100.0));
        leveling.GainXP(-loseXP);

        // Inform server, that we have died - so that monsters doens't attack us
        ClientSend.PlayerDied();

        UIManager.Instance.DeadScreenManager.SwitchDeadScreen();
    }
    public void Respawn()
    {
        alive = true;

        animator.Play("Idle");
        ClientSend.PlayAnimation("Idle");

        UIManager.Instance.DeadScreenManager.SwitchDeadScreen();

        HandlePlayerHealthbar(maxHealth/2, maxMana/2);

        ClientSend.PlayerRespawn();
    }
    void SetFocus(Interactable newFocus) 
    {
        if (newFocus != focus) {
            if (focus != null)
                focus.OnDefocused();
            focus = newFocus;
        }
        newFocus.OnFocused(transform);
    }
    void RemoveFocus() 
    {
        if (focus != null)
            focus.OnDefocused();
        focus = null;
    }
}
