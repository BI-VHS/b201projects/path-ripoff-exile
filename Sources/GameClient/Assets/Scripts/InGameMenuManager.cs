﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameMenuManager : MonoBehaviour
{
    public void ChangeToDevroomScene()
    {
        GameManager.Instance.ChangeScene(Constants.SceneType.Devroom);
        UIManager.Instance.SwitchInGameMenu();
    }

    public void ChangeToMountainScene()
    {
        GameManager.Instance.ChangeScene(Constants.SceneType.Mountains);
        UIManager.Instance.SwitchInGameMenu();
    }

    public void ChangeToCityScene()
    {
        GameManager.Instance.ChangeScene(Constants.SceneType.City);
        UIManager.Instance.SwitchInGameMenu();
    }

    public void ChangeToPlainsScene()
    {
        GameManager.Instance.ChangeScene(Constants.SceneType.Plains);
        UIManager.Instance.SwitchInGameMenu();
    }

    public void ChangeToWastelandScene()
    {
        GameManager.Instance.ChangeScene(Constants.SceneType.Wasteland);
        UIManager.Instance.SwitchInGameMenu();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
