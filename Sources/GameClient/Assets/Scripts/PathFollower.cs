﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFollower : MonoBehaviour
{
    public float speed = 3f;
    public float rotationSpeed = 30f;
	public Transform pathParent;
	Transform targetPoint;
	int index;
    public bool follow = false;

	void Start()
    {
		index = 0;
		targetPoint = pathParent.GetChild(index);
	}
	
	// Update is called once per frame
	void Update()
    {
        // At the end of path
        if(index >= pathParent.childCount || !follow)
        {
            return;
        }

        // Move & rotate toward it
		transform.position = Vector3.MoveTowards(transform.position, targetPoint.position, speed * Time.deltaTime);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetPoint.rotation, rotationSpeed * Time.deltaTime);

        // Get next point in path if we are close
        if(Vector3.Distance(transform.position, targetPoint.position) < 0.1f)
		{
			index++;
            if(index < pathParent.childCount)
            {
                targetPoint = pathParent.GetChild(index);
            }
		}
	}
}
