﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickMenuManager : MonoBehaviour
{
    public void OpenInventoryWindow()
    {
        LocalPlayerManager.Instance.inventory.gameObject.GetComponentInChildren<InventoryUI>().OpenInventoryWindow();
    }
    public void OpenCharacterWindow()
    {
        LocalPlayerManager.Instance.inventory.gameObject.GetComponentInChildren<CharacterScreenUI>().OpenCharacterWindow();
    }
    public void OpenQuestWindow()
    {
        LocalPlayerManager.Instance.inventory.gameObject.GetComponentInChildren<QuestUI>().OpenQuestWindow();
    }
    public void OpenMapWindow()
    {
        UIManager.Instance.MapManager.SwitchMap();
    }

    void Update()
    {
        if(!UIManager.Instance.ChatManager.InputFieldFocused())
        {
            if(Input.GetKeyDown(KeyCode.M))
                OpenMapWindow();

            else if(Input.GetKeyDown(KeyCode.I))
                OpenInventoryWindow();

            else if(Input.GetKeyDown(KeyCode.U))
                OpenCharacterWindow();

            else if(Input.GetKeyDown(KeyCode.J))
                OpenQuestWindow();
        }
        
    }
}
