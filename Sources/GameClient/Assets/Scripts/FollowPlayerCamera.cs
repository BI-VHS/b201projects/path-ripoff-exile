﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerCamera : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if(LocalPlayerManager.Instance != null)
        {
            transform.position = LocalPlayerManager.Instance.transform.position + new Vector3(0.0f, 10.0f, -8.0f);
            transform.LookAt(LocalPlayerManager.Instance.transform);
        }
    }
}
