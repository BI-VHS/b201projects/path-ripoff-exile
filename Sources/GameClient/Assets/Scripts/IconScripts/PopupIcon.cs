﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupIcon : MonoBehaviour
{
    public float radius = 9.0f;
    private Animator animator;    
    private bool isNearby;
    void Start()
    {
        animator = this.gameObject.GetComponentInChildren<Animator>();
        isNearby = false;
    }
    void Update()
    {
        bool wasNearby = isNearby;
        isNearby = PlayerIsNearby();

        if(wasNearby != isNearby)
            ChangeState();
    }
    private void ChangeState()
    {
        if (animator != null)
        {
            bool isActive = animator.GetBool("isActive");
            animator.SetBool("isActive", !isActive);
        }
    }
    private bool PlayerIsNearby()
    {
        if (LocalPlayerManager.Instance != null)
        {
            float distance = Vector3.Distance(LocalPlayerManager.Instance.transform.position, transform.position);

            if (distance < radius)
                return true;
        }
        return false;
    }
}
