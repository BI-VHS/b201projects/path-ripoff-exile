﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    private bool hasMap = false;
    public void SwitchMap()
    {
        hasMap = false;
        
        for (int i = 0; i < LocalPlayerManager.Instance.inventory.items.Count; i++)
        {
            if(LocalPlayerManager.Instance.inventory.items[i] is Map)
            {
                hasMap = true;
                break;
            }
        }
        if(hasMap)
        {
            bool isActive = gameObject.activeSelf;
            gameObject.SetActive(!isActive);
            SoundManager.Instance.Play("MapSound");
        }
        else
        {
            string message = "You don't have a Map in your inventory.";
            GameManager.Instance.SendMessage(LocalPlayerManager.Instance.username, message);
        }
    }
}
