﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Map")]
public class Map : Item
{
    public override void Use()
    {
        UIManager.Instance.MapManager.SwitchMap();
    }
}
