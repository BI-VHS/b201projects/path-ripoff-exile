﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/ManaPotion")]
public class ManaPotion : Item
{
    public int mana;
    override public void Use()
    {
        LocalPlayerManager.Instance.UpdateMana(mana);
        RemoveFromInventory();
    }
}
