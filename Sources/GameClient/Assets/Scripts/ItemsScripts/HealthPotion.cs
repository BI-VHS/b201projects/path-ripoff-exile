﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/HealthPotion")]
public class HealthPotion : Item
{
    public int health;
    override public void Use()
    {
        LocalPlayerManager.Instance.UpdateHealth(health);
        RemoveFromInventory();
    }
}
