﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class MainMenuManager : MonoBehaviour
{
    public GameObject navigation;
    public GameObject loginMenu;
    public InputField usernameField;
    public InputField serverIpField;
    public InputField portField;
    public GameObject chooseHero;

    private void Start()
    {
        // Check to see, if all in-editor dependecies are correctly setup
        Assert.IsNotNull(navigation);
        Assert.IsNotNull(loginMenu);
        Assert.IsNotNull(usernameField);
        Assert.IsNotNull(serverIpField);
        Assert.IsNotNull(portField);

        loginMenu.gameObject.SetActive(false);
        chooseHero.SetActive(false);
    }

    
    private void Update()
    {
        // Cast a ray a get new position where to move
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Input.GetMouseButtonDown(0))
        {
            if(Physics.Raycast(ray, out hit))
            {
                if(hit.transform.gameObject.layer == LayerMask.NameToLayer("NPC"))
                {
                    Debug.Log("HITTED!");
                }
            }
        }
    }

    public void ConnectToServer()
    {
        loginMenu.SetActive(false);
        chooseHero.SetActive(false);
        usernameField.interactable = false;
        serverIpField.interactable = false;
        portField.interactable = false;
        Client.Instance.ConnectToServer();

        // Inform GameManagerand UI manager about change of the scene
        UIManager.Instance.ChangeScene(Constants.SceneType.City);
    }

    // After player presses button in main menu show him login screen with character choices
    public void GoToLogin()
    {
        SwitchNavAndLogin();
        Camera.main.GetComponent<PathFollower>().follow = true;
        chooseHero.SetActive(true);
        chooseHero.GetComponent<Animation>().Play("PanelFadeIn");

    }

    private void SwitchNavAndLogin()
    {
        if(navigation.activeSelf)
        {
            navigation.SetActive(false);
            loginMenu.gameObject.SetActive(true);
        }
        else
        {
            navigation.SetActive(true);
            loginMenu.gameObject.SetActive(false);
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
