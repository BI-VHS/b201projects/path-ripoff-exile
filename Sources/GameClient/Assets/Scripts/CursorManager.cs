﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    public Texture2D swordHoverTexture;
    public Texture2D handHoverTexture;
    public Texture2D swordClickTexture;

    void Start()
    {
        ActivateHandHoverCursor();   
    }
    public void ActivateHandHoverCursor()
    {
        Cursor.SetCursor(handHoverTexture, Vector2.zero, CursorMode.Auto);
    }
    public void ActivateSwordHoverCursor()
    {
        Cursor.SetCursor(swordHoverTexture, Vector2.zero, CursorMode.Auto);
    }
    public void ActivateSwordClickCursor()
    {
        Cursor.SetCursor(swordClickTexture, Vector2.zero, CursorMode.Auto);
    }
}
