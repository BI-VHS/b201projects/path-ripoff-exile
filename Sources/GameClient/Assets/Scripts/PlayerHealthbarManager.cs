﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerHealthbarManager : MonoBehaviour
{
    public Slider healthSlider;
    public Slider manaSlider;
    public Image iconWarrior;
    public Image iconMage;
    public Image iconThief;
    public Image iconMedic;
    public TextMeshProUGUI levelText;
    public void SetIcon(Constants.CharacterClass characterClass)
    {
        switch(characterClass)
        {
            case Constants.CharacterClass.Warrior:
                iconWarrior.gameObject.SetActive(true);      
                break;
            case Constants.CharacterClass.Mage:
                iconMage.gameObject.SetActive(true);         
                break;
            case Constants.CharacterClass.Thief:
                iconThief.gameObject.SetActive(true);        
                break;
            case Constants.CharacterClass.Medic:
                iconMedic.gameObject.SetActive(true);        
                break;
            default:
                break;
        }
    }
    public void SetMaxHealth(int health)
    {
        healthSlider.maxValue = health;
    }
    public void SetMaxMana(int mana)
    {
        manaSlider.maxValue = mana;
    }
    public void SetHealth(int health)
    {
        healthSlider.value = health;
    }
    public void SetMana(int mana)
    {
        manaSlider.value = mana;
    }
    public void SetLevel(int level)
    {
        levelText.text = level.ToString();
    }
}
