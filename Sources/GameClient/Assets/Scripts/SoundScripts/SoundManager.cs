﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    public Sound [] sounds;
    void Start()
    {
        foreach(Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.playOnAwake = s.playOnAwake;
            s.source.volume = s.volume;
            s.source.loop = s.loop;
        }
    }
    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sounds => sounds.name == name);

        if(s != null)
            s.source.Play();
    }
}
