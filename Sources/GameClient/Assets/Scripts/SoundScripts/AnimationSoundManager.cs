﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSoundManager : MonoBehaviour
{
    public AudioClip clipDie;
    public AudioClip [] clipAttack;
    protected AudioSource source;
    void Start()
    {
        source = this.gameObject.GetComponent<AudioSource>();
    }
    public void Play()
    {
        if(source.clip != null)
            source.Play();
    }
    public void DieSound()
    {
        source.clip = clipDie;
        Play();
    }
    public void AttackSound()
    {
        int index = Random.Range (0, clipAttack.Length);

        source.clip = clipAttack[index];
        Play();
    }
}
