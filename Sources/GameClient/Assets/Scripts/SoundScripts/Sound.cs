﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound 
{
    public string name;
    public AudioClip clip;
    public bool playOnAwake;
    public bool loop;

    [Range(0f, 1f)]
    public float volume = 0.5f;

    [HideInInspector]
    public AudioSource source; 
}
