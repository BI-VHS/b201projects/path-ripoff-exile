﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundManager : AnimationSoundManager
{
    public AudioClip clipAbility;
    public void AbilitySound()
    {
        source.clip = clipAbility;
        Play();
    }
}
