﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreenManager : MonoBehaviour
{
    public GameObject loadingScreen;
    public Slider slider;

    void Start()
    {
        loadingScreen.gameObject.SetActive(false);
    }
    public void LoadLevel(Constants.SceneType _scene)
    {
        StartCoroutine(LoadAsync(_scene));
    }
    
    IEnumerator LoadAsync(Constants.SceneType _scene)
    {
        loadingScreen.gameObject.SetActive(true);

        AsyncOperation operation = ChangeScene(_scene);
        
        if(operation != null)
        {
            slider.value = 0.0f;

            while (!operation.isDone)
            {
                float progress = Mathf.Clamp01(operation.progress / .9f);
                Debug.Log(progress);
                slider.value = progress;

                yield return null;
            }
        }
        loadingScreen.gameObject.SetActive(false);
    }
    private AsyncOperation ChangeScene(Constants.SceneType _scene)
    {
        AsyncOperation operation;

        switch(_scene)
        {
            case Constants.SceneType.Devroom:
                operation = SceneManager.LoadSceneAsync("Devroom");
                break;
            case Constants.SceneType.Mountains:
                operation = SceneManager.LoadSceneAsync("MountainsTest");
                break;
            case Constants.SceneType.City:
                operation = SceneManager.LoadSceneAsync("city2");
                break;
            case Constants.SceneType.Plains:
                operation = SceneManager.LoadSceneAsync("Plains");
                break;
            case Constants.SceneType.Wasteland:
                operation = SceneManager.LoadSceneAsync("WastelandTest");
                break;
            default:
                operation = null;
                break;
        }
        return operation;
    }
}
