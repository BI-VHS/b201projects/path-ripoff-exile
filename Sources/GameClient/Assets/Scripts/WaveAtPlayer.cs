﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformData
{
    public Vector3 LocalPosition = Vector3.zero;
    public Vector3 LocalEulerRotation = Vector3.zero;
    public Vector3 LocalScale = Vector3.one;

    // Unity requires a default constructor for serialization
    public TransformData() { }

    public TransformData(Transform transform)
    {
        LocalPosition = transform.localPosition;
        LocalEulerRotation = transform.localEulerAngles;
        LocalScale = transform.localScale;
    }

    public void ApplyTo(Transform transform)
    {
        transform.localPosition = LocalPosition;
        transform.localEulerAngles = LocalEulerRotation ;
        transform.localScale = LocalScale;
    }
}
public class WaveAtPlayer : MonoBehaviour
{
    public float    radius;
    Animator        animator;
    private TransformData originalTransform;


    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        originalTransform = new TransformData(transform);
    }

    // Update is called once per frame
    void Update()
    {
        if (LocalPlayerManager.Instance != null)
        {
            float distance = Vector3.Distance(LocalPlayerManager.Instance.transform.position,transform.position);

            if (distance < radius)
            {
                animator.SetBool("isWaving", true);
                Vector3 forward = LocalPlayerManager.Instance.transform.position - transform.position;

                Quaternion lookAt = Quaternion.LookRotation(forward);
                transform.rotation = Quaternion.Slerp(transform.rotation, lookAt,Time.deltaTime*5.0f);
            }
            else
            {
                animator.SetBool("isWaving", false);

                if (transform.localEulerAngles != originalTransform.LocalEulerRotation)
                {
                    Quaternion origRotation = Quaternion.Euler(originalTransform.LocalEulerRotation);
                    transform.rotation = Quaternion.Slerp(transform.rotation, origRotation ,Time.deltaTime*5.0f);
                }
            }

        }
    }
}
