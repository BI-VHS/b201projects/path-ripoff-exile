﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAbilityManager : MonoBehaviour
{
    public Ability [] abilities;

    public Ability GetAbility(int ability)
    {
        return abilities[ability];
    }
    public void PlayAbility(int ability)
    {
        abilities[ability].particle.Play();
    }
    public void StopAbility(int ability)
    {
        abilities[ability].particle.Stop();
    }
    public void StopAll()
    {
        foreach(Ability a in abilities)
            a.particle.Stop();
    }
}
