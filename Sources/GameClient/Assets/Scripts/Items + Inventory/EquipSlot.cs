﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipSlot : MonoBehaviour
{
    public Image icon;
    public Sprite originalIcon;
    public Text stats;

    Equipment equip;

    void Start()
    {
        stats.enabled = false;
    }

    public void AddItemToSlot( Equipment newEquip )
    {
        equip = newEquip;
        icon.sprite = equip.icon;
        stats.enabled = true;
        
        stats.text = "";
        if (newEquip.armorModifier != 0)
            stats.text = "ARM +" + newEquip.armorModifier.ToString() + "\n";
        if (newEquip.damageModifier != 0)
            stats.text += "DMG +" + newEquip.damageModifier.ToString();
    }

    public void ClearSlot()
    {
        equip = null;
        icon.sprite = originalIcon;
        stats.enabled = false;
    }

    public void OnClickButton()
    {
        if (equip == null)
            return;

        int slotIndex = (int)equip.equipSlot;
        LocalPlayerManager.Instance.equip.Unequip(slotIndex);
    }
}
