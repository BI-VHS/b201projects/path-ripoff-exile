﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemPickup : Interactable {

	public List<Item> item;	// Item(s) to put in the inventory if picked up

	// When the player interacts with the item
	public override void Interact()
	{
		base.Interact();
		PickUp();
	}
	// Pick up the item
	void PickUp ()
	{	
		// iterate through the items, if player has the inventory space for all of them -> destroy the gamobject
		for (int i = item.Count-1; i >= 0; i--)
		{
			bool enoughSpace = LocalPlayerManager.Instance.inventory.Add(item[i]);
        	if (enoughSpace) 
			{
				GameManager.Instance.SendItemMessage(LocalPlayerManager.Instance.username, item[i].name);

				// If we picked up a quest item -> update the count
                for (int j = 0; j < LocalPlayerManager.Instance.questing.quests.Count; j++)
                {
                    LocalPlayerManager.Instance.questing.quests[j].ProgressMade(item[i]);

                    if (LocalPlayerManager.Instance.questing.quests[j].IsReached())
                        LocalPlayerManager.Instance.questing.quests[j].finished = true;

                    LocalPlayerManager.Instance.questing.UpdateQuestWindow();
                }
				item.RemoveAt(i);
			}
			else 
			{
				//inventory full
				break;
			}

			// picked all items from object
			if ( i == 0 )
				Destroy(gameObject);	// Destroy object from scene
		}
	}

}









/*
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemPickup : Interactable {

	public Item[] item;	// Item(s) to put in the inventory if picked up

	// When the player interacts with the item
	public override void Interact()
	{
		base.Interact();

		PickUp();
	}


	// Pick up the item
	void PickUp ()
	{	
		for (int i = 0; i < item.Length; i++)
		{
			bool enoughSpace = LocalPlayerManager.Instance.inventory.Add(item[i]);
        	if (enoughSpace) {
				Debug.Log("Picking up " + item[i].name);
			}
			else
				break;

			if ( i == (item.Length - 1) )
				Destroy(gameObject);	// Destroy object from scene
		}
	}

}
*/