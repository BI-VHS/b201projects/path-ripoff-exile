﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManager : MonoBehaviour
{
    public Equipment[] currentEquipment;

    public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem);
    public OnEquipmentChanged onEquipmentChanged;

    // Start is called before the first frame update
    void Start()
    {
        int numSlots = System.Enum.GetNames(typeof(EquipmentSlot)).Length;
        currentEquipment = new Equipment[numSlots];
    }

    public void Equip( Equipment newItem ) 
    {
        // correct index (Head = 0, Chest = 1, ...)
        int slotIndex = (int)newItem.equipSlot;
        Equipment oldItem = null;

        // if something is already equiped -> switch it
        if (currentEquipment[slotIndex] != null)
        {
            oldItem = currentEquipment[slotIndex];
            LocalPlayerManager.Instance.inventory.Add(oldItem);
        }

        //callback if something was equiped
        if (onEquipmentChanged != null)
            onEquipmentChanged.Invoke(newItem, oldItem);
        
        // equip the new item
        currentEquipment[slotIndex] = newItem;
    }

    public void Unequip( int slotIndex )
    {
        if (currentEquipment[slotIndex] != null)
        {
            Equipment oldItem = currentEquipment[slotIndex];
            bool enoughSpace = LocalPlayerManager.Instance.inventory.Add(oldItem);

            if (enoughSpace) {
                //callback if something was unequiped
                if (onEquipmentChanged != null)
                    onEquipmentChanged.Invoke(null, oldItem);

                // unequip the item
                currentEquipment[slotIndex] = null;
            }
        }
    }
}
