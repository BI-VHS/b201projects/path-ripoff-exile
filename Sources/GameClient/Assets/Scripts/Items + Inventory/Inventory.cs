﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Inventory : MonoBehaviour
{

    // called when an item is added or removed from inventory
    public delegate void OnItemChanged();
	public OnItemChanged onItemChangedCallback;

    public int space = 25;       // number of slots
    public int gold;
    public List<Item> items = new List<Item>();
    public QuestItem questItemRef;

    public bool Add(Item item) {
        if (items.Count >= space) {
            Debug.Log("Not enough space in inventory");
            return false;
        }

        items.Add(item);
        SoundManager.Instance.Play("ItemSound");
        
        if (onItemChangedCallback != null)
			onItemChangedCallback.Invoke ();

        return true;
    }

    public bool Remove(Item item) 
    {
        if (item.GetType() != questItemRef.GetType())
        {
            items.Remove(item);
            SoundManager.Instance.Play("ItemSound");

            if (onItemChangedCallback != null)
                onItemChangedCallback.Invoke();
        }

        return true;
    }

    public bool RemoveQuestItem(Item questItem)
    {
        for (int i = 0; i < LocalPlayerManager.Instance.questing.quests.Count; i++)
        {
            if (LocalPlayerManager.Instance.questing.quests[i].IsReached())
                LocalPlayerManager.Instance.questing.quests[i].finished = true;

            LocalPlayerManager.Instance.questing.UpdateQuestWindow();
        }
        items.Remove(questItem);

        if (onItemChangedCallback != null)
                onItemChangedCallback.Invoke();

        return true;
    }

    public void GainGold(int _amount)
    {
        gold += _amount;
    }
}
