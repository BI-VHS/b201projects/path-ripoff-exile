﻿  
using UnityEngine;

/* The base item class. All items should derive from this. */

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Item")]
public class Item : ScriptableObject {

	new public string name = "New Item";	// Name of the item
	public Sprite icon = null;				// Item icon
	public int drop_percentage = 100;       // 0 - 100

	// Called when the item is pressed in the inventory
	public virtual void Use ()
	{
		// Use the item
		Debug.Log("Using " + name);
	}

	public void RemoveFromInventory()
	{
		LocalPlayerManager.Instance.inventory.Remove(this);
	}

}
