﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHighlight : MonoBehaviour
{
    public Color highlightColor = Color.red;
    private Color startColor;
    private Renderer rend;
    void Start() 
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
    }
    void OnMouseEnter()
    {
        rend.material.color = highlightColor;
    }
    void OnMouseExit() 
    {
        rend.material.color = startColor;
    }
}