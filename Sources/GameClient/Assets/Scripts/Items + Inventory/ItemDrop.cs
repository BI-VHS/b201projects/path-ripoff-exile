﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ItemDrop : MonoBehaviour
{
    public List<Item> drop;
    private float roll;
    private GameObject drop_crate;
    private GameObject orig_mesh;
    private ItemPickup items;

    // Start is called before the first frame update
    void Start()
    {
        items = GetComponent<ItemPickup>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.X))
        {
            roll = Random.Range(0.0f, 100.0f);

            for (int i = drop.Count-1; i >= 0; i--) {
                if (drop[i].drop_percentage > roll) {
                    print(drop[i]);
                    items.item.Add(drop[i]);
                }
            }

            // enemy mesh has to be first child
            //orig_mesh = gameObject.transform.GetChild(0).gameObject;
            // drop crate has to be second child
            //drop_crate = gameObject.transform.GetChild(1).gameObject;
            //drop_crate = transform.parent.gameObject;

            if (gameObject != null) {
                print("Setting " + gameObject + " active");
                //orig_mesh.SetActive(false);
                //drop_crate.SetActive(true);
                gameObject.SetActive(true);
            }
        }
    }
}
