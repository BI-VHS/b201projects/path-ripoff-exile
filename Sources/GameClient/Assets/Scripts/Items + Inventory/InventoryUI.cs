﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    public Transform itemsParent;
    public GameObject inventoryUI;

    InventorySlot[] slots;

    // Start is called before the first frame update
    void Start()
    {
        LocalPlayerManager.Instance.inventory.onItemChangedCallback += UpdateUI;
    
        slots = itemsParent.GetComponentsInChildren<InventorySlot>();
        inventoryUI.SetActive(false);
    }
    public void OpenInventoryWindow()
    {
        SoundManager.Instance.Play("WindowSound");
        inventoryUI.SetActive(!inventoryUI.activeSelf);
    }
    
    void UpdateUI() {
        for (int i = 0; i < slots.Length; i++)
        {
            if (i < LocalPlayerManager.Instance.inventory.items.Count) {
                slots[i].AddItemToSlot(LocalPlayerManager.Instance.inventory.items[i]);
            }
            else {
                slots[i].ClearSlot();
            }
        }
    }
}
