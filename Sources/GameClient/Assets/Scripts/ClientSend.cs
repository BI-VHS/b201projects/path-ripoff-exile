﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientSend : MonoBehaviour
{
    // Send a packet to the server over TCP
    private static void SendTCPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.Instance.tcp.SendData(_packet);
    }
    // Send a packet to the server over UDP
    private static void SendUDPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.Instance.udp.SendData(_packet);
    }

    // Response to server's welcome message
    public static void WelcomeReceived()
    {
        using(Packet _packet = new Packet((int)ClientPackets.welcomeReceived))
        {
            _packet.Write(Client.Instance.myId);
            _packet.Write(UIManager.Instance.MainMenuManager.usernameField.text);
            _packet.Write((int) GameManager.Instance.chosenCharacterClass);
            SendTCPData(_packet);
        };
    }

    // Here using UDP for sending our position to server. Repeated a lot and we don't care if something gets lost
    public static void PlayerMovement(Vector3 _newDestination)
    {
        using(Packet _packet = new Packet((int)ClientPackets.playerMovement))
        {
            _packet.Write(_newDestination);
            SendUDPData(_packet);
        }
    }

    // Sending to server our message that we posted into the chat.
    public static void SendChatMessage(ChatMessage _message)
    {
        using(Packet _packet = new Packet((int)ClientPackets.sendChatMessage))
        {
            _packet.Write(_message.Date);
            _packet.Write(_message.Username);
            _packet.Write(_message.Text);
            SendTCPData(_packet);
        }
    }

    // Sending to server that we want to change the scene
    public static void ChangeScene(Constants.SceneType _newScene)
    {
        using(Packet _packet = new Packet((int)ClientPackets.changeScene))
        {
            _packet.Write((int) _newScene);
            SendTCPData(_packet);
        }
    }

    // Inform server, that we want to reload scene that we are currently in
    public static void PlayerRespawn()
    {
        using(Packet _packet = new Packet((int)ClientPackets.playerRespawn))
        {
            SendTCPData(_packet);
        }
    }

    // Inform server, that we have made attack on given monster by its ID and we have done this amount of damage
    public static void DealDamageToMonster(int _monsterId, int _damage)
    {
        using(Packet _packet = new Packet((int)ClientPackets.dealDamageToMonster))
        {
            _packet.Write((int) _monsterId);
            _packet.Write((int) _damage);
            SendTCPData(_packet);
        } 
    }

    // Send impulse to all other clients that are in my scene, that i have started playing given animation via string
    public static void PlayAnimation(string _animation)
    {
        using(Packet _packet = new Packet((int)ClientPackets.playAnimation))
        {
            _packet.Write(_animation);
            SendUDPData(_packet);
        } 
    }

    // Send information to server, that player with given ID has died
    public static void PlayerDied()
    {
        using(Packet _packet = new Packet((int)ClientPackets.playerDied))
        {
            SendTCPData(_packet);
        } 
    }
}
