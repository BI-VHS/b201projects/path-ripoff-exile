﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelingManager : MonoBehaviour
{
    public int playerLevel = 1;
    public int currentExperience;
    public int neededExperience;

    void Start()
    {
        playerLevel = 1;
        currentExperience = 0;
        neededExperience = 200;
    }

    void Update()
    {
        if (currentExperience >= neededExperience)
            LevelUp();
    }

    public void GainXP(int _amount)
    {
        currentExperience += _amount;

        if(currentExperience < 0)
            currentExperience = 0;
    }

    public void LevelUp() 
    {
        currentExperience = 0;
        playerLevel++;
        neededExperience = (int)(neededExperience * 1.25);

        // boost player stats
        LocalPlayerManager.Instance.maxHealth = (int)(LocalPlayerManager.Instance.maxHealth * 1.1);
        LocalPlayerManager.Instance.health = LocalPlayerManager.Instance.maxHealth;
        LocalPlayerManager.Instance.maxMana = (int)(LocalPlayerManager.Instance.maxMana * 1.1);
        LocalPlayerManager.Instance.mana = LocalPlayerManager.Instance.maxMana;
        LocalPlayerManager.Instance.basicAttackDamage += 1;
        LocalPlayerManager.Instance.basicArmorValue += 1;
        LocalPlayerManager.Instance.basicStrength += 1;
        LocalPlayerManager.Instance.basicIntellect += 1;
        LocalPlayerManager.Instance.basicSpeed += 1;

        UIManager.Instance.PlayerHealthbarManager.SetMaxHealth(LocalPlayerManager.Instance.maxHealth);
        UIManager.Instance.PlayerHealthbarManager.SetHealth(LocalPlayerManager.Instance.maxHealth);

        UIManager.Instance.PlayerHealthbarManager.SetMaxMana(LocalPlayerManager.Instance.maxMana);
        UIManager.Instance.PlayerHealthbarManager.SetMana(LocalPlayerManager.Instance.maxMana);

        UIManager.Instance.PlayerHealthbarManager.SetLevel(playerLevel);
    }
}
