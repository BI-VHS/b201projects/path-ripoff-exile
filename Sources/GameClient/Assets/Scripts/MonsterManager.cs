﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.AI;

public class MonsterManager : Interactable
{
    public Constants.MonsterType type;
    public int id;
    public int health;
    public int damage;
    public int xpDropAmount;
    public int goldDropAmount;
    public GameObject healthbar;
    private MonsterHealthbarManager healthbarManager;
    private NavMeshAgent navMeshAgent;
    private Animator animator;
    private AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = this.gameObject.GetComponent<NavMeshAgent>();
        animator = this.gameObject.GetComponent<Animator>();
        source = this.gameObject.GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        if(navMeshAgent.destination == transform.position)
        {
            animator.SetBool("isWalking", false);
        }
    }
    public void SetHealth(int _health)
    {
        health = _health;
        healthbarManager.SetHealth(_health);
    }
    public void SetHealthbar(MonsterHealthbarManager _healthbarManager, int health)
    {
        healthbarManager = _healthbarManager;
        healthbarManager.SetMaxHealth(health);
    }
    public void UpdateDestination(Vector3 _newDestination)
    {
        navMeshAgent.destination = _newDestination;
        animator.SetBool("isWalking", true);
    }
    public void TakeDamage(int _dealtDamage)
    {
        health -= _dealtDamage;
        healthbarManager.SetHealth(health);
    }
    public void Attack()
    {
        animator.Play("Attack");
    }
    public void Die()
    {
        // Stop moving
        navMeshAgent.enabled = false;
        animator.Play("Die");
    }
}
