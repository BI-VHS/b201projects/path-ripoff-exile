﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class FindChildRecursively
{
    public static Transform FindChild(Transform parent, string name)
    {
        Transform findChild = parent.Find(name);

        if(findChild != null)
            return findChild;

        foreach(Transform child in parent)
        {
            Transform findGrandChild = FindChild(child, name);

            if(findGrandChild != null)
                return findGrandChild;
        }
        return null;
    }

}
