﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ChatMessage
{
    private string date;
    private string username;
    private string text;

    public string Date
    {
        get{ return date; }
    }

    public string Username
    {
        get{ return username; }
    }

    public string Text
    {
        get{ return text; }
    }
    
    // Constructor
    public ChatMessage(string _date, string _username, string _text)
    {
        date = _date;
        username = _username;
        text = _text;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
