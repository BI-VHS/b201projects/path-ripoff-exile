﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class WalkAndSit : MonoBehaviour
{
    public Transform[] points;
    public float sitTime;
    public float waitTime;
    public string[] pointAction;

    private int destPoint = 0;
    private NavMeshAgent agent;
    private Animator animator;
    private bool sitting;
    private bool waiting;
    

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        agent.autoBraking = false;

        GotoNextPoint();
    }

    void Update()
    {
        if (agent.isStopped == false) {

            agent.updateRotation = true;

            if(!agent.pathPending && agent.remainingDistance < 0.5f)
                GotoNextPoint();

            if (pointAction[destPoint] == "Sit" && agent.remainingDistance < 0.5f) {
                StartCoroutine("Sit");
                animator.SetBool("isSitting", true);
                animator.SetBool("isWaiting", false);
                //return;
            }
            else if (pointAction[destPoint] == "Wait" && agent.remainingDistance < 0.5f) {
                StartCoroutine("Wait");
                animator.SetBool("isWaiting", true);
                animator.SetBool("isSitting", false);
                //return;
            }
            else {
                animator.SetBool("isWaiting", false);
                animator.SetBool("isSitting", false);
                //return;
            }
        }
        if (sitting) {
            animator.SetBool("isSitting", true);
            agent.isStopped = true;
        }
        else if (waiting) {
            animator.SetBool("isWaiting", true);
            agent.isStopped = true;

            agent.updateRotation = false;
            Vector3 toCenter = new Vector3(1,0,0);
            Quaternion lookAt = Quaternion.LookRotation(toCenter);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookAt,Time.deltaTime*5.0f);
        }
        else {
            animator.SetBool("isSitting", false);
            animator.SetBool("isWaiting", false);
            agent.isStopped = false;
        }

    }

    IEnumerator Sit() 
    {
        agent.isStopped = true;
        sitting = true;

        // Wait and do nothing while sitting
        yield return new WaitForSeconds(sitTime);
        agent.isStopped = false;
        sitting = false;
    }
    IEnumerator Wait() 
    {
        agent.isStopped = true;
        waiting = true;

        // Wait and do nothing while waiting
        yield return new WaitForSeconds(waitTime);
        agent.isStopped = false;
        waiting = false;
    }

    void GotoNextPoint()
    {
        if (points.Length == 0)
            return;

        agent.destination = points[destPoint].position;
        destPoint = (destPoint + 1) % points.Length;
    }
}
