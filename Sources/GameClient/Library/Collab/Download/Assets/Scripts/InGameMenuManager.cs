﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameMenuManager : MonoBehaviour
{
    public void ChangeToDevroomScene()
    {
        GameManager.Instance.ChangeScene("Devroom");
        UIManager.Instance.SwitchInGameMenu();
    }

    public void ChangeToMountainScene()
    {
        GameManager.Instance.ChangeScene("MountainsTest");
        UIManager.Instance.SwitchInGameMenu();
    }

    public void ChangeToCityScene()
    {
        GameManager.Instance.ChangeScene("city2");
        UIManager.Instance.SwitchInGameMenu();
    }

    public void ChangeToNatureScene()
    {
        GameManager.Instance.ChangeScene("");
        UIManager.Instance.SwitchInGameMenu();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
