﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

/* This class defines CLIENT-SIDE receiving of TCP packets.
 * This has to be the same, as in ServerSend class!!!
 */

public class ClientHandle : MonoBehaviour
{
    // Test welcome packet from the server, read and store values from obtained packet
    public static void Welcome(Packet _packet)
    {
        string _msg = _packet.ReadString();
        int _myId = _packet.ReadInt();
        

        Debug.Log($"Message from server: {_msg}");
        Client.Instance.myId = _myId;
        ClientSend.WelcomeReceived();

        // TCP connection done, initiate UDP
        Client.Instance.udp.Connect(((IPEndPoint)Client.Instance.tcp.socket.Client.LocalEndPoint).Port);
    }

    // CLIENT-SIDE receiving info about new player
    public static void SpawnPlayer(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _username = _packet.ReadString();
        Vector3 _position = _packet.ReadVector3();
        Quaternion _rotation = _packet.ReadQuaternion();
        GameManager.Instance.SpawnPlayer(_id, _username, _position, _rotation);
    }

    // One of the other clients on the server has moved and we obtained their id and position from server
    public static void PlayerMoved(Packet _packet)
    {
        int id = _packet.ReadInt();
        Vector3 newDestination = _packet.ReadVector3();

        // Change the destination of nav mesh agent of given player
        GameManager.players[id].UpdateDestination(newDestination);
    }

    // One of the other clients on the server has sent a message.
    // Add it to our local chat manager and register the message/
    public static void ReceiveChatMessage(Packet _packet)
    {
        string date = _packet.ReadString();
        string username = _packet.ReadString();
        string text = _packet.ReadString();

        // Add to local chat manager
        GameManager.Instance.RegisterChatMessage(new ChatMessage(date, username, text));
    }


    // Delete disconnected player from our local copy of all players in dictionary
    public static void PlayerDisconnected(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Destroy(GameManager.players[_id].gameObject);
        GameManager.players.Remove(_id);
    }
}
