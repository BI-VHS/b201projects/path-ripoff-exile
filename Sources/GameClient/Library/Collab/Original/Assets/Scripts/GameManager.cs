﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using TMPro;

public class GameManager : Singleton<GameManager>
{
    // Class variables
    // Storing information about all other players on CLIENT-SIDE
    public static Dictionary<int, RemotePlayerManager> players;
    public static Dictionary<int, MonsterManager> monsters;
    public List<GameObject> monsterPrefabs;
    public ChatManager chatManager;
    public GameObject [] localPlayerClassPrefabs;
    public GameObject [] remotePlayerClassPrefabs;
    public Constants.CharacterClass chosenCharacterClass;

    // Initialization, using singleton here (only one instance throught out hte program)
    private void Start()
    {
        players = new Dictionary<int, RemotePlayerManager>();
        monsters = new Dictionary<int, MonsterManager>();

        // Check to see, if all in-editor dependecies are correctly setup
        Assert.IsNotNull(chatManager);
    }

    // Spawning self and other players and keeping track of all in a dictionary
    public void SpawnPlayer(int _id, string _username, Constants.CharacterClass _characterClass, Vector3 _position, Quaternion _rotation)
    {
        // Show HUD
        UIManager.Instance.ShowHud();

        // Spawn myself
        if(_id == Client.Instance.myId && LocalPlayerManager.Instance == null)
        {
            // Creating local player - SINGLETON
            Instantiate(localPlayerClassPrefabs[(int) _characterClass], _position, _rotation);
            LocalPlayerManager.Instance.GetComponent<LocalPlayerManager>().id = _id;
            LocalPlayerManager.Instance.GetComponent<LocalPlayerManager>().username = _username;
            LocalPlayerManager.Instance.GetComponent<LocalPlayerManager>().characterClass = _characterClass;
            LocalPlayerManager.Instance.GetComponentInChildren<TextMeshProUGUI>().text = _username;
        }
        // Spawn other player
        else
        {
            GameObject remotePlayer;
            remotePlayer = Instantiate(remotePlayerClassPrefabs[(int) _characterClass], _position, _rotation);
            remotePlayer.GetComponent<RemotePlayerManager>().id = _id;
            remotePlayer.GetComponent<RemotePlayerManager>().username = _username;
            remotePlayer.GetComponentInChildren<TextMeshProUGUI>().text = _username;
            players.Add(_id, remotePlayer.GetComponent<RemotePlayerManager>());
        }
    }

    public void RemotePlayerMovement(int _id, Vector3 _newDestination)
    {
        // Change the destination of nav mesh agent of given player
        // Take into account only clients, that are in our scene
        if(players.ContainsKey(_id))
        {
            players[_id].UpdateDestination(_newDestination);
        }
    }

    public void MonsterMovement(int _id, Vector3 _newDestination)
    {
        // Change the destination of nav mesh agent of given monster
        // Double check that the monster is in our scene
        if(monsters.ContainsKey(_id))
        {
            monsters[_id].UpdateDestination(_newDestination);
        }
    }

    public void SendChatMessage(string _username)
    {
        chatManager.SendChatMessage(_username);
    }

    public void RegisterChatMessage(ChatMessage _username)
    {
        chatManager.RegisterChatMessage(_username);
    }

    // Handles the scene change and comunnication with server to all other clients
    public void ChangeScene(Constants.SceneType _scene)
    {
        // Change scene
        UIManager.Instance.ChangeScene(_scene);

        // Clear out dictionary with remote player managers
        players.Clear();

        // Clear out dictionary with all monsters
        monsters.Clear();

        // Inform other clients via server about our change
        ClientSend.ChangeScene(_scene);
    }

    // One of the client from server was in our scene and left it. Delete his RemotePlayerManager Gameobject
    public void DeleteRemotePlayer(int _clientIdToDelete)
    {
        if(players.ContainsKey(_clientIdToDelete))
        {
            Destroy(players[_clientIdToDelete].gameObject);
            players.Remove(_clientIdToDelete);
        }
    }

    // Create new Monster game object and assign the randomly generated
    // values to it (generated on the server).
    public void SpawnMonster(Constants.MonsterType _monsterType, int _id, Vector3 _position, Quaternion _rotation)
    {
        // Lookup correct prefab
        GameObject prefab = monsterPrefabs.Find(tmp => tmp.GetComponent<MonsterManager>().type == _monsterType);
        GameObject monster = Instantiate(prefab, _position, _rotation);
        MonsterManager manager = monster.GetComponent<MonsterManager>();

        GameObject monsterHealthbarPrefab = manager.healthbar;
        GameObject healthbar = Instantiate(monsterHealthbarPrefab, _position + Vector3.up * 2, Quaternion.identity);
        healthbar.transform.SetParent(monster.transform);

        manager.type = _monsterType;
        manager.id = _id;
        
        MonsterHealthbarManager healthbarManager = FindChildRecursively.FindChild(healthbar.transform, "SliderHealth").GetComponent<MonsterHealthbarManager>();
        manager.SetHealthbar(healthbarManager, manager.health);

        monsters.Add(_id, manager);
    }

    // Find monster by its ID and change it health value appropriatly
    public void MonsterTookDamage(int _id, int _dealtDamage)
    {
        // Ignore other received packets from monsters that are not in our scene
        if(monsters.ContainsKey(_id))
        {
            MonsterManager monster = monsters[_id];
            monster.TakeDamage(_dealtDamage);
        }
    }

    // Find monster by its ID and delete it
    public void MonsterDied(int _id)
    {
        // Ignore other received packets from monsters that are not in our scene
        if(monsters.ContainsKey(_id))
        {
            Destroy(monsters[_id].gameObject);
            monsters.Remove(_id);
        }
    }

    // Remote client has played animation on their end. Play it also for us
    public void PlayerPlayedAnimation(int _playerId, string _animation)
    {
        players[_playerId].animator.Play(_animation);
    }

    // Update our health values, take damage and play monster attack animation
    public void MonsterAttacked(int _monsterId, int _damageTaken)
    {
        // Ignore other received packets from monsters that are not in our scene
        if(monsters.ContainsKey(_monsterId))
        {
            LocalPlayerManager.Instance.TakeDamage(_damageTaken);
            monsters[_monsterId].Attack();
        }
    }
}
