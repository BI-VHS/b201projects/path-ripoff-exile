﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientSend : MonoBehaviour
{
    // Send a packet to the server over TCP
    private static void SendTCPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.Instance.tcp.SendData(_packet);
    }
    // Send a packet to the server over UDP
    private static void SendUDPData(Packet _packet)
    {
        _packet.WriteLength();
        Client.Instance.udp.SendData(_packet);
    }

    // Response to server's welcome message
    public static void WelcomeReceived()
    {
        using(Packet _packet = new Packet((int)ClientPackets.welcomeReceived))
        {
            _packet.Write(Client.Instance.myId);
            _packet.Write(UIManager.Instance.MainMenuManager.usernameField.text);
            SendTCPData(_packet);
        };
    }

    // Here using UDP for sending our position to server. Repeated a lot and we don't care if something gets lost
    public static void PlayerMovement(Vector3 _newDestination)
    {
        using(Packet _packet = new Packet((int)ClientPackets.playerMovement))
        {
            _packet.Write(_newDestination);
            SendUDPData(_packet);
        }
    }

    // Sending to server our message that we posted into the chat.
    public static void SendChatMessage(ChatMessage _message)
    {
        using(Packet _packet = new Packet((int)ClientPackets.sendChatMessage))
        {
            _packet.Write(_message.Date);
            _packet.Write(_message.Username);
            _packet.Write(_message.Text);
            SendTCPData(_packet);
        }
    }
}
