﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RemotePlayerManager : MonoBehaviour
{
    public int id;
    public string username;
    public Animator animator;
    public NavMeshAgent navMeshAgent;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Updates the target destination for nav mesh agent
    public void UpdateDestination(Vector3 _newDestination)
    {
        navMeshAgent.destination = _newDestination;
    }
}
