﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.Assertions;

public class LocalPlayerManager : Singleton<LocalPlayerManager>
{
    public int id;
    public string username;
    private Animator animator;
    public NavMeshAgent navMeshAgent;
    public Interactable focus;
    public Inventory inventory; 
    public EquipmentManager equip;
    public float attackRange;
    public int basicAttackDamage;
    public GameObject attackTarget;
    public Constants.CharacterClass characterClass;

    //particles
    public ParticleSystem particles1;

    public int maxMana = 100;
    public int currentMana;

    //private variables for ability
    private uint currentAbility = 0;
    

    //times
    private float manaTime = 0.0f; //last time when was mana restored
    private float ability1Time = 0.0f; //last time when ability was used

    //consts
    private const int ability1Cost = 10;

    private void Start()
    {
        Assert.IsNotNull(navMeshAgent);
        animator = GetComponentInChildren<Animator>();
        inventory = GetComponentInChildren<Inventory>();
        equip = GetComponentInChildren<EquipmentManager>();

        UIManager.Instance.PlayerHealthbarManager.SetMaxMana(maxMana);
        UIManager.Instance.PlayerHealthbarManager.SetIcon(characterClass);

        currentMana = maxMana;
    }

    private void Update()
    {
        if (Input.GetKey("1") && currentMana > ability1Cost)
        {
            if(Time.time - ability1Time > 1.0f)    
                HandleAbility1();
        }
        else
        {
            
            if (currentAbility > 0)
            {
                //saying you no longer doing any ability
                currentAbility = 0;
                // Play animation
                animator.Play("Idle");
                // Play animation for all other clients via server
                ClientSend.PlayAnimation("Idle");
                particles1.Stop();
            }

            HandleMovement();
            HandleChat();
            HandleInGameMenu();
            HandleBasicAttack();
            HandleRestoreMana();
        }

        // Switch back to idle if not walking
        if(navMeshAgent.destination == transform.position)
        {
            animator.SetBool("isWalking", false);
        }
    }

    // Changes the destination point of navigation of player upon mouse click
    private void HandleMovement()
    {
        // User clicked on some UI element, don't move
        if(EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        // Cast a ray a get new position where to move
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        // Mouse right click
        if(Input.GetMouseButtonDown(1))
        {
            if(Physics.Raycast(ray, out hit, 100))
            {
                UpdateDestination(hit.point);

                animator.SetBool("isWalking", true);
                
                // Inform the server about new destination
                ClientSend.PlayerMovement(hit.point);

                // User clicked on monster, that was not previously clicked
                if(hit.collider.gameObject.CompareTag("Monster"))
                {
                    attackTarget = hit.collider.gameObject;
                }
                else
                {
                    attackTarget = null;
                }

                // if clicked on an item, enemy, etc. -> interact with it
                Interactable interactable = hit.collider.GetComponent<Interactable>();
                if(interactable != null)
                {
                    if(focus != interactable)
                    {
                        print("Setting focus on " + interactable);
                        SetFocus(interactable);
                    }
                }
                else
                {
                    RemoveFocus();
                }
            }
        }
    }

    // If user presses 'enter' send a new message to chat
    private void HandleChat()
    {
        if(Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            GameManager.Instance.SendChatMessage(this.username);
        }
    }

    // If user presses 'esc' inform UIManager that it should bring up in game menu
    private void HandleInGameMenu()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            UIManager.Instance.SwitchInGameMenu();
        }
    }

    // Checks to see if any valid attack target is set and player is close enough to attack it
    // Player is attacking via basic auto attack (right click on enemy)
    private void HandleBasicAttack()
    {
        // Verify, that we have clicked on something with tag Monster and it has MonsterManager attach to it
        if(attackTarget != null && attackTarget.GetComponent<MonsterManager>() != null)
        {
            if(Vector3.Distance(transform.position, attackTarget.transform.position) <= attackRange)
            {
                // Play animation
                animator.Play("Attack");

                // Play animation for all other clients via server
                ClientSend.PlayAnimation("Attack");

                // Calculate dealt damage - TODO as of now base damage as constant
                int damage = basicAttackDamage;

                // Get monster's id from monster manager attached to attacked target
                int monsterId = attackTarget.GetComponent<MonsterManager>().id;

                // Send info about the attack and dealt damage to server, where it monster's health is adjusted
                // and other Clients are informed
                ClientSend.DealDamageToMonster(monsterId, damage);

                // Reset target after one attack is done
                attackTarget = null;
            }
        }
    }


    private void HandleAbility1()
    {
        ability1Time = Time.time;
        //if you wasnt doing ability1 start doing it
        if (currentAbility != 1)
        {
            print(currentMana);
            currentAbility = 1;
            particles1.Play();
            // Play animation
            animator.Play("Ability1");
            // Play animation for all other clients via server
            ClientSend.PlayAnimation("Ability1");
        }

         //pro Maru bylo by lepsi udelat na serveru nejakou funkci co umi menit animator bool Ability pro remote playera, bude to vic smooth

        //Stop the movement
        UpdateDestination(transform.position);
        //Stop the movement for server
        ClientSend.PlayerMovement(transform.position);

        currentMana -= ability1Cost;
        UIManager.Instance.PlayerHealthbarManager.SetMana(currentMana);

        switch (characterClass)
        {
            case Constants.CharacterClass.Warrior:
                //do warrior ability
                //taunt enemies and higher up defense
                break;
            case Constants.CharacterClass.Mage:
                //do mage ability
                break;
            case Constants.CharacterClass.Thief:
                //do thief ability
                break;
            case Constants.CharacterClass.Medic:
                //do medic ability
                break;
            default:
                break;
        }
    }

    private void HandleRestoreMana()
    {
        if (currentMana < maxMana && Time.time - manaTime >= 10.0f)
        {
            print("Mana restored!! current mana is now " + currentMana);
            manaTime = Time.time;
            currentMana += 10;
            
            UIManager.Instance.PlayerHealthbarManager.SetMana(currentMana);

            switch (characterClass)
            {
                case Constants.CharacterClass.Warrior:
                    currentMana += 10;

                    break;
                case Constants.CharacterClass.Mage:
                    //do mage ability
                    break;
                case Constants.CharacterClass.Thief:
                    //do thief ability
                    break;
                case Constants.CharacterClass.Medic:
                    //do medic ability
                    break;
                default:
                    break;
            }
        }
        else if (currentMana > maxMana)
            currentMana = maxMana;
    }

    // Updates the target destination for nav mesh agent
    public void UpdateDestination(Vector3 _newDestination)
    {
        navMeshAgent.destination = _newDestination;
    }

    public void UpdatePosition(Vector3 _newPosition)
    {
        navMeshAgent.Warp(_newPosition);
    }

    void SetFocus(Interactable newFocus) {
        if (newFocus != focus) {
            if (focus != null)
                focus.OnDefocused();
            focus = newFocus;
        }
        newFocus.OnFocused(transform);
    }

    void RemoveFocus() {
        if (focus != null)
            focus.OnDefocused();
        focus = null;
    }
}
