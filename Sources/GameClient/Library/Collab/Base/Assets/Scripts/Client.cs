﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;

/* This is CLIENT-SIDE CLIENT class. Please note, that changes in here
 * has to also match changes in SERVER-SIDE CLIENT class.
 */

public class Client : Singleton<Client>
{
    public static int dataBufferSize = 4096;
    // Default values for server, can be changed while running
    public string ip = "127.0.0.1";
    public int port = 26950;
    public int myId = 0;
    public TCP tcp;
    public UDP udp;
    private bool isConnected = false;
    private delegate void PacketHandler(Packet _packet);
    private static Dictionary<int, PacketHandler> packetHandlers;


    // Initialization of dictionary for packet handlers
    // due to network & packet segmentation
    private void InitializeClientData()
    {
        packetHandlers = new Dictionary<int, PacketHandler>()
        {
            {(int)ServerPackets.welcome, ClientHandle.Welcome},
            {(int)ServerPackets.spawnPlayer, ClientHandle.SpawnPlayer},
            {(int)ServerPackets.playerMoved, ClientHandle.PlayerMoved},
            {(int)ServerPackets.newChatMessage, ClientHandle.ReceiveChatMessage},
            {(int)ServerPackets.playerDisconnected, ClientHandle.PlayerDisconnected},
        };
        Debug.Log("Initialized packets.");
    }

    // Cleanup after closing the game
    private void OnApplicationQuit()
    {
        Disconnect();
    }

    // Start the TCP connection to the server
    public void ConnectToServer()
    {
        // Initialize IP address, by default 127.0.0.1 (localhost)
        ip = UIManager.Instance.MainMenuManager.serverIpField.text;
        port = int.Parse(UIManager.Instance.MainMenuManager.portField.text); // TODO check if int
        tcp = new TCP();
        udp = new UDP();
        isConnected = true;
        InitializeClientData();
        tcp.Connect();
    }

    // Handles the disconnection form clients side
    private void Disconnect()
    {
        if(isConnected)
        {
            isConnected = false;
            tcp.socket.Close();
            udp.socket.Close();
            Debug.Log("Disconnected from the server.");
        }
    }

    // -------------------------------------------------------------------------------
    // Helper subclass for TCP/IP networking stuff
    public class TCP
    {
        // Class variables
        public TcpClient socket;
        private NetworkStream stream;
        private Packet receivedData;
        private byte[] receiveBuffer;

        // CLIENT-SIDE client connection to server
        public void Connect()
        {
            // Socket initialization
            socket = new TcpClient
            {
                ReceiveBufferSize = dataBufferSize,
                SendBufferSize = dataBufferSize
            };
            receiveBuffer = new byte[dataBufferSize];
            socket.BeginConnect(Instance.ip, Instance.port, ConnectCallback, socket);
        }

        // Called upon valid connection to a server
        private void ConnectCallback(IAsyncResult _result)
        {
            socket.EndConnect(_result);
            if(!socket.Connected)
            {
                return;
            }
            // Prepare stream & start reading
            stream = socket.GetStream();
            receivedData = new Packet();
            stream.BeginRead(receiveBuffer, 0, dataBufferSize, ReceiveCallback, null);
        }

        // Send a packet to the server
        public void SendData(Packet _packet)
        {
            try
            {
                if(socket != null)
                {
                    stream.BeginWrite(_packet.ToArray(), 0, _packet.Length(), null, null);
                }
            }
            catch(Exception _ex)
            {
                Debug.Log($"Error sending data to server via TCP: {_ex}");
            }
        }

        // Called upon obtaining some kind of data by the server
        private void ReceiveCallback(IAsyncResult _result)
        {
            try
            {
                int _byteLength = stream.EndRead(_result);
                // Calling Client's disconnect, closing socket
                if (_byteLength <= 0)
                {
                    Instance.Disconnect();
                    return;
                }
                byte[] _data = new byte[_byteLength];
                Array.Copy(receiveBuffer, _data, _byteLength);

                // Handle received data. Reset if all bytes arrived have been handled
                receivedData.Reset(HandleData(_data));

                stream.BeginRead(receiveBuffer, 0, dataBufferSize, ReceiveCallback, null);
            }
            catch (Exception _ex)
            {
                Console.WriteLine($"Error receiving TCP data: {_ex}");
                Disconnect();
            }
        }

        // This is pretty complicated function.
        // Basically it makes sure, that the data is handled correctly, if network
        // segmentation happens and for example we only receive a half of a packet.
        private bool HandleData(byte[] _data)
        {
            int _packetLength = 0;
            receivedData.SetBytes(_data);

            if(receivedData.UnreadLength() >= 4)
            {
                _packetLength = receivedData.ReadInt();
                // Reset received data
                if(_packetLength <= 0)
                {
                    return true;
                }
            }
            // Received data contains another full packet that we can handle
            while(_packetLength > 0 && _packetLength <= receivedData.UnreadLength())
            {
                // Copy bytes from another packet to temporary byte array
                byte[] _packetBytes = receivedData.ReadBytes(_packetLength);

                // Make sure, that this code runs on ONE thread using our thread manager
                ThreadManager.ExecuteOnMainThread(() =>
                {
                    // Load next packet from socket. Select appropriate packet handler
                    using (Packet _packet = new Packet(_packetBytes))
                    {
                        int _packetId = _packet.ReadInt();
                        packetHandlers[_packetId](_packet);
                    }
                });
                // Reset packet length
                _packetLength = 0;

                // Check again if there are any other packets to be handled else return true for
                // received data to reset causing getting more data
                if (receivedData.UnreadLength() >= 4)
                {
                    _packetLength = receivedData.ReadInt();
                    // Reset received data
                    if (_packetLength <= 0)
                    {
                        return true;
                    }
                }
            }
            if(_packetLength <= 1)
            {
                return true;
            }
            // DON'T reset received data, some part of packet still left
            return false;
        }

        // Disconnect on TCP level
        private void Disconnect()
        {
            // Calling Client's disconnect, closing socket
            Instance.Disconnect();
            stream = null;
            receivedData = null;
            receiveBuffer = null;
            socket = null;
        }
    }

    // -------------------------------------------------------------------------------
    // Helper subclass for UDP/IP networking stuff
    public class UDP
    {
        public UdpClient socket;
        public IPEndPoint endPoint;

        // Constructor
        public UDP()
        {
            endPoint = new IPEndPoint(IPAddress.Parse(Instance.ip), Instance.port);
        }

        // CLIENT-SIDE client connection to server
        public void Connect(int _localPort)
        {
            socket = new UdpClient(_localPort);
            socket.Connect(endPoint);
            socket.BeginReceive(ReceiveCallback, null);

            // Send empty packet to server with our ID to initiate the connection
            using(Packet _packet = new Packet())
            {
                SendData(_packet);
            }
        }

        // Send a packet to the server
        public void SendData(Packet _packet)
        {
            try
            {
                // Add our client ID to the packet. Server needs to know who send it
                // Server is running only one UDP client socket
                _packet.InsertInt(Instance.myId);
                if(socket != null)
                {
                    socket.BeginSend(_packet.ToArray(), _packet.Length(), null, null);
                }
            }
            catch(Exception ex)
            {
                Debug.Log($"Error sending data to server via UDP: {ex}");
            }
        }

        // Called upon obtaining some kind of data by the server
        private void ReceiveCallback(IAsyncResult _result)
        {

            try
            {
                // Store data and start receiveing again
                byte[] _data = socket.EndReceive(_result, ref endPoint);
                socket.BeginReceive(ReceiveCallback, null);
                // Calling Client's disconnect, closing socket
                if (_data.Length < 4)
                {
                    Instance.Disconnect();
                    return;
                }
                HandleData(_data);
            }
            catch (Exception _ex)
            {
                Console.WriteLine($"Error receiving TCP data: {_ex}");
                Disconnect();
            }
        }

        // TODO: segmentation of UDP packets may be a source of problems
        private void HandleData(byte[] _data)
        {
            // Shorten the byte array to it doesn't start with length
            using(Packet _packet = new Packet(_data))
            {
                int _packetLength = _packet.ReadInt();
                _data = _packet.ReadBytes(_packetLength);
            }
            // Store the received packet with appropriate packet handler
            ThreadManager.ExecuteOnMainThread(() =>
            {
                using(Packet _packet = new Packet(_data))
                {
                    int _packetId = _packet.ReadInt();
                    packetHandlers[_packetId](_packet);
                }
            });
        }

        // Calling Client's disconnect, closing socket
        private void Disconnect()
        {
            Instance.Disconnect();
            endPoint = null;
            socket = null;
        }
    }
}
