﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class UIManager : Singleton<UIManager>
{
    private MainMenuManager mainMenuManager;
    public MainMenuManager MainMenuManager
    {
        get{ return mainMenuManager; }
    }
    private InGameMenuManager inGameMenuManager;
    public InGameMenuManager InGameMenuManager
    {
        get{ return inGameMenuManager; }
    }
    private HudManager hudManager;
    public HudManager HudManager
    {
        get{ return hudManager; }
    }

    void Start()
    {
        mainMenuManager = GetComponentInChildren<MainMenuManager>();
        inGameMenuManager = GetComponentInChildren<InGameMenuManager>();
        hudManager = GetComponentInChildren<HudManager>();

        // First show main menu, hide everything else
        inGameMenuManager.gameObject.SetActive(false);
        hudManager.gameObject.SetActive(false);
    }

    public void ChangeScene(string _sceneName)
    {
        SceneManager.LoadScene(_sceneName);
    }

    public void ShowHud()
    {
        hudManager.gameObject.SetActive(true);
    }

    public void SwitchInGameMenu()
    {
        if(inGameMenuManager.gameObject.activeSelf)
        {
            inGameMenuManager.gameObject.SetActive(false);
        }
        else
        {
            inGameMenuManager.gameObject.SetActive(true);
        }
    }
}
