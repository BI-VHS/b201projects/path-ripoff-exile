# Custom networking engine

## Basic information

We have decided to use previous implementation of custom networking engine and build upon it. It is based upon a tutorial from Tom Weiland (Youtube channel link:https://www.youtube.com/channel/UCa-mDKzV5MW_BXjSDRqqHUw[here]) and is heavily customised.

We have also analysed basic problems, that you can encounter while creating a Server / Client infrastructure, such as lag compensation, client side prediction, authorative server etc. and to make our work easier, we have decided to move a lot of the responsibility to Client.

We are completely aware, that by doing this we enable hackers and cheaters to manipulate with the game a ruin the game for others, however it is not in our scope of work for this project to handle. It is possible, that in future we will completely restructuralize the infrastructure but as of now we are not planning it.

## Features
- Usage of TCP and UDP ports to send packets back and forth
- Custom Packet class (able to sent int, bool, vectors, quaternions over the network)
- Client & Server handles (reaction to received packets from the other end)
- Basic process of Client connecting to the Server implemented including exchange of IDs
- Movement and rotation is client-sided. Server is only a relay that sends player's new transformation to all other clients.

## Client Server connection flow diagram

image::https://gitlab.fit.cvut.cz/BI-VHS/b201projects/path-ripoff-exile/raw/master/Documentation/images/diagrams/server_client_connect_diagram.png[]